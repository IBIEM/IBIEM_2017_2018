Long Running Chunks

When you have a long running chunk, sometime the RStudio interface doesn’t act appropriately, here are some tricks and bits of information that I have found useful:


Even though a chunk looks like it is still running, it might not be.  To check this, you can run “top” in the terminal panel, this shows what is running, sorted so that the most computationally intensive process is on top.  So if you run a chunk with “split_libraries_fastq.py”, then look at “top”, it should show “split_libraries_fastq.py” at the top of the list with “%CPU” somewhere around 99%.  When “split_libraries_fastq.py” drops off that list, it usually means its done, even if RStudio doesn’t reflect this.  One caveat is that a command can hang, then it drops off the top list, but might not be done.  I usually check to see if the output file has been created and looks complete.  In addition, most QIIME commands generate a log file, so you can look at these to see if they are done.

If a chunk is done, but RStudio isn’t recognizing that fact, you might need to "kick it”, there are several ways to do this, from least to most extreme.  Before you do any of these, be sure that any open files are saved:
1. Close the notebook, then reopen it.
2. “Restart R and Clear Output” from the “Session” menu.
3. “Quit Session” under “File” menu

If the chunk you were waiting for was outputting results to a file, and that was successful, you won’t need to run it again, but you may need to run other chunks that set up variables that you need for other chunks (e.g. paths and filenames).

In general I like to get each chunk working with a representative subset of the data, then once I have the whole RMarkdown pipeline set up, switch to the full dataset, and run the whole thing at once using one of:
1. the “Knit to HTML” option from the “knit” menu
2. Run this command from the terminal: R -e "rmarkdown::render(‘path_to_rmarkdown_file/rmarkdown_file_to_run.Rmd’)"
3. If you really want to play it safe, you can do #2 within a tmux session - I’ll let you google tmux

