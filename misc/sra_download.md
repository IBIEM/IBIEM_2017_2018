Replace "/sharedspace/platypus\_poop/raw\_data" with the path for your raw data Replace "SRR390728" with your accession number

``` r
raw.data="/sharedspace/platypus_poop/raw_data"
Sys.setenv(RAW_DATA=raw.data)
Sys.setenv(SRA_ACC="SRR390728")
dir.create(raw.data, recursive = TRUE)
```

    ## Warning in dir.create(raw.data, recursive = TRUE): '/sharedspace/
    ## platypus_poop/raw_data' already exists

Quick test of fastq-dump. Should output the first 3 reads from SRR390728

``` bash
fastq-dump --maxSpotId 3 --stdout  $SRA_ACC
```

    ## Read 3 spots for SRR390728
    ## Written 3 spots for SRR390728
    ## @SRR390728.1 1 length=72
    ## CATTCTTCACGTAGTTCTCGAGCCTTGGTTTTCAGCGATGGAGAATGACTTTGACAAGCTGAGAGAAGNTNC
    ## +SRR390728.1 1 length=72
    ## ;;;;;;;;;;;;;;;;;;;;;;;;;;;9;;665142;;;;;;;;;;;;;;;;;;;;;;;;;;;;;96&&&&(
    ## @SRR390728.2 2 length=72
    ## AAGTAGGTCTCGTCTGTGTTTTCTACGAGCTTGTGTTCCAGCTGACCCACTCCCTGGGTGGGGGGACTGGGT
    ## +SRR390728.2 2 length=72
    ## ;;;;;;;;;;;;;;;;;4;;;;3;393.1+4&&5&&;;;;;;;;;;;;;;;;;;;;;<9;<;;;;;464262
    ## @SRR390728.3 3 length=72
    ## CCAGCCTGGCCAACAGAGTGTTACCCCGTTTTTACTTATTTATTATTATTATTTTGAGACAGAGCATTGGTC
    ## +SRR390728.3 3 length=72
    ## -;;;8;;;;;;;,*;;';-4,44;,:&,1,4'./&19;;;;;;669;;99;;;;;-;3;2;0;+;7442&2/

Check if `--origfmt` gives more useful header Compare this to above

``` bash
fastq-dump --maxSpotId 3 --stdout --origfmt $SRA_ACC
```

    ## Read 3 spots for SRR390728
    ## Written 3 spots for SRR390728
    ## @1
    ## CATTCTTCACGTAGTTCTCGAGCCTTGGTTTTCAGCGATGGAGAATGACTTTGACAAGCTGAGAGAAGNTNC
    ## +1
    ## ;;;;;;;;;;;;;;;;;;;;;;;;;;;9;;665142;;;;;;;;;;;;;;;;;;;;;;;;;;;;;96&&&&(
    ## @2
    ## AAGTAGGTCTCGTCTGTGTTTTCTACGAGCTTGTGTTCCAGCTGACCCACTCCCTGGGTGGGGGGACTGGGT
    ## +2
    ## ;;;;;;;;;;;;;;;;;4;;;;3;393.1+4&&5&&;;;;;;;;;;;;;;;;;;;;;<9;<;;;;;464262
    ## @3
    ## CCAGCCTGGCCAACAGAGTGTTACCCCGTTTTTACTTATTTATTATTATTATTTTGAGACAGAGCATTGGTC
    ## +3
    ## -;;;8;;;;;;;,*;;';-4,44;,:&,1,4'./&19;;;;;;669;;99;;;;;-;3;2;0;+;7442&2/

Do a test run: download the first five reads and save to compressed FASTQ

``` bash
fastq-dump --maxSpotId 5 --split-files --gzip --outdir $RAW_DATA $SRA_ACC
ls -ltr $RAW_DATA
```

    ## Read 5 spots for SRR390728
    ## Written 5 spots for SRR390728
    ## total 8
    ## -rw-r--r-- 1 guest users 255 Feb 16 23:07 SRR390728_2.fastq.gz
    ## -rw-r--r-- 1 guest users 271 Feb 16 23:07 SRR390728_1.fastq.gz

Download the full dataset

``` bash
fastq-dump --split-files --gzip --outdir $RAW_DATA $SRA_ACC
ls -ltr $RAW_DATA
```

Don't forget to set to read-only

``` bash
chomd -R a-w $RAW_DATA
ls -ltrd $RAW_DATA
```
