# Accessing IBIEM Computing Environment
For IBIEM bootcamp and course we will be using a web based version RStudio which is hosted by Duke’s Office of Information Technology.  To access your instance:

1. Go to [VM Manage](https://vm-manage.oit.duke.edu/containers)
2. Logon with your NetID and Password
4. Find the section "IBIEM2017 - RStudio for bootcamp, CSP1, and CSP2 2017"
5. Click on "Click here to create your personal IBIEM2017 environment"
6. RStudio should open in your webbrowser

If at any point reason you get to a "Sign in to RStudio" webpage that asks for your Username and Password, you will need to go back through the [VM Manage](https://vm-manage.oit.duke.edu/containers).  This sometimes happens after restarting your browser or losing your connection to computing environment.
