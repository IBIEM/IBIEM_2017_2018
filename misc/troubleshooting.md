No space left on device
=======================

Getting this error message: `OSError: [Errno 28] No space left on device`?

Solutions
---------

### Change TMP directory

Add (and run) the following chunk before the chunk that is giving you the error (the top of your noteboook is a good place for this). This seems to work for QIIME2 (the only place this problem has popped up so far).

``` r
tmp.dir = path.expand("~/tmp")

Sys.setenv(TMPDIR=tmp.dir)
dir.create(tmp.dir, recursive = TRUE, showWarnings = FALSE)
```

> #### TLDR
>
> Apparently, QIIME2 likes to put a lot of intermediate files in `/tmp`. This is generally quite reasonable, but that space is limited in computing environment, so we need to put those intermediate files someplace more spaceous.

### Clean up /tmp

Even if the above fixes your problem, its a good idead to be sure that /tmp isn't clogged with junk, because other programs (e.g rstudio) put stuff there.

How full is `/tmp`? You can check with the following command. If you see 100% (or something close to it, like 99%) under `Use%`, then `/tmp` is full.

``` bash
df -h /tmp
```

    ## Filesystem      Size  Used Avail Use% Mounted on
    ## /dev/sde1        50G   45G  2.1G  96% /tmp

If it is full, please go through the following steps to clean up any large files that you no longer need. Note that /tmp is in a space that is shared by everyone, so it might not be your fault. If you clean up your `/tmp` and it doesn't significantly decrease `Use%`, then let the TAs and/or instructors know so that we can initiate a class-wide cleanup.

The following command shows you all the subdirectories in your `/tmp`, sorted by size from largest to smallest. The only subdirectories here are from R (starting with `Rtmp`), Rstudio (`rstudio-rsession`), and tmux (`tmux-1000`). We don't want to mess with any of these directories, and anyway, they aren't that big - the sizes are listed in kb. The only large directory is `/tmp` itself (102432KB is about 100MB), which means that there is one or more large files directly in `/tmp`.

``` bash
du --max-depth 1 /tmp | sort -nr
```

    ## 102436   /tmp
    ## 8    /tmp/rstudio-rsession
    ## 4    /tmp/tmux-1000
    ## 4    /tmp/RtmpMaRhIz
    ## 4    /tmp/RtmpKvUHoa
    ## 4    /tmp/RtmpJLiFFz
    ## 4    /tmp/RtmpHGcLJz

This command shows you all the files in your `/tmp`, sorted by size from largest to smallest. We want to get rid of any file we don't need, but particularly that one at the top that is 100M. &gt; If you are uncertain what can safely be deleted, feel free to check with a TAs and/or instructor.

``` bash
ls -lShap /tmp | grep -v / 
```

    ## total 101M
    ## -rw-r--r--   1 guest users 100M Apr 18 21:42 big_file.txt
    ## -rw-r--r--   1 guest users   20 Apr 18 21:25 small_file.txt

This command will do that for us. We can also do it from the Rstudio file pane by checking the box and clicking "Delete" in the "Files" pane

``` bash
rm -f /tmp/big_file.txt
```

### Clean up /home/guest

We also want to check our home directory. If you run the following chunk and see 100% (or something close to it, like 99%) under `Use%`, then the problem is that all of our space is full. Please clean up any large files that you no longer need, and if that doesn't significantly decrease `Use%`, then let the TAs and/or instructors know so that we can initiate a class-wide cleanup.

``` bash
df -h /home/guest
```

    ## Filesystem      Size  Used Avail Use% Mounted on
    ## /dev/sdd1      1008G  942G   16G  99% /home/guest

As with `/tmp`, we can use `du` and `ls` to find large files.

It looks like I could stand to clean up some

``` bash
du --max-depth 1 ~/ | sort -nr
```

    ## 464884   /home/guest/
    ## 425940   /home/guest/ssh_IBIEM_2017_2018
    ## 24564    /home/guest/output
    ## 4380 /home/guest/IBIEM_2017_2018
    ## 3428 /home/guest/ncbi
    ## 1236 /home/guest/inclass_notes
    ## 1176 /home/guest/R
    ## 1112 /home/guest/2017_2018_bootcamp
    ## 868  /home/guest/.rstudio
    ## 536  /home/guest/planets
    ## 532  /home/guest/my_project
    ## 412  /home/guest/.config
    ## 344  /home/guest/.cache
    ## 296  /home/guest/git_intro
    ## 20   /home/guest/.ssh
    ## 4    /home/guest/tmp

It doesn't look like I have any large files in my home directory!

``` bash
ls -lShap ~/ | grep -v / 
```

    ## total 100K
    ## -rw-------  1 guest users 8.9K Apr 18 21:43 .bash_history
    ## -rw-r--r--  1 guest users 3.6K Apr 18 21:36 .Rhistory
    ## -rw-------  1 guest users 1.7K Feb 16 23:09 .viminfo
    ## -rw-r--r--  1 guest users  340 Mar 29 21:59 .wget-hsts
    ## -rw-r--r--  1 guest users   72 Feb 19 20:20 .gitconfig
    ## -rw-------  1 guest users   55 Feb 27 20:58 .lesshst
