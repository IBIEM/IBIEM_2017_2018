<!-- 
R -e "rmarkdown::render('misc/argonne_download.Rmd', output_format=c('html_document', 'md_document'))" 
R -e "rmarkdown::render('misc/argonne_download.Rmd')" 
-->
Load Libraries
==============

``` r
suppressPackageStartupMessages(library(ShortRead))
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(knitr))
```

Set up directory to receive data
================================

``` r
raw.data="/sharedspace/its_and_18s_data"
Sys.setenv(RAW_DATA=raw.data)
dir.create(raw.data, recursive = TRUE, showWarnings = FALSE)
```

Download data
=============

``` bash
wget --continue --recursive --no-verbose --no-host-directories --cut-dirs=2 \
  --directory-prefix $RAW_DATA \
  ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/
```

    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/ [283] -> "/sharedspace/its_and_18s_data/.listing" [1]
    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180223_McCumber_ITS_fastqs/ [401] -> "/sharedspace/its_and_18s_data/180223_McCumber_ITS_fastqs/.listing" [1]
    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_I1_001.fastq.gz [324360119] -> "/sharedspace/its_and_18s_data/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_I1_001.fastq.gz" [1]
    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R1_001.fastq.gz [3368075229] -> "/sharedspace/its_and_18s_data/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R1_001.fastq.gz" [1]
    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R2_001.fastq.gz [3912180012] -> "/sharedspace/its_and_18s_data/180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R2_001.fastq.gz" [1]
    ## 2018-03-07 02:32:32 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180228_McCumber_18s_fastqs/ [401] -> "/sharedspace/its_and_18s_data/180228_McCumber_18s_fastqs/.listing" [1]
    ## 2018-03-07 02:32:33 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180228_McCumber_18s_fastqs/Undetermined_S0_L001_I1_001.fastq.gz [271724509] -> "/sharedspace/its_and_18s_data/180228_McCumber_18s_fastqs/Undetermined_S0_L001_I1_001.fastq.gz" [1]
    ## 2018-03-07 02:32:33 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180228_McCumber_18s_fastqs/Undetermined_S0_L001_R1_001.fastq.gz [1678909139] -> "/sharedspace/its_and_18s_data/180228_McCumber_18s_fastqs/Undetermined_S0_L001_R1_001.fastq.gz" [1]
    ## 2018-03-07 02:32:33 URL: ftp://ftp.igsb.anl.gov/jobs/23ce422d165831c252e02c100340fdda/180228_McCumber_18s_fastqs/Undetermined_S0_L001_R2_001.fastq.gz [1635634404] -> "/sharedspace/its_and_18s_data/180228_McCumber_18s_fastqs/Undetermined_S0_L001_R2_001.fastq.gz" [1]
    ## FINISHED --2018-03-07 02:32:33--
    ## Total wall clock time: 1.9s
    ## Downloaded: 6 files, 10G in 0.006s (1724 GB/s)

Sanity Checks
=============

Check that:

1.  FASTQs have same number of lines
2.  FASTQs have same first read ID
3.  FASTQs have same last read ID

``` r
fastq_stats = function(fastq.file){
  # cat("File:", fastq.file, fill = TRUE)
  stream <- FastqStreamer(fastq.file, 100000)
  firstchunk = yield(stream)
  fqcount = length(firstchunk)
  first_read = toString(ShortRead::id(firstchunk[1]))
  # cat("First Read:", first_read, fill = TRUE)

  while (length(fq <- yield(stream))) {
    lastchunk = fq
    fqcount = fqcount + length(fq)
  }
  close(stream)
  last_read = toString(ShortRead::id(lastchunk[length(lastchunk)]))
  # cat("Last Read:", last_read, fill = TRUE)
  # cat("Read Count:", fqcount, fill = TRUE)
  return(c(fastq=basename(fastq.file),
              num_reads = fqcount,
              first_read_id = first_read, 
              last_read_id = last_read))
}
```

``` r
for (curdir in list.files(raw.data, full.names = TRUE)) {
  fastq_qc = lapply(list.files(curdir, full.names = TRUE), fastq_stats)
  fastq_qc.df = bind_rows(lapply(fastq_qc, as.data.frame.list))

  cat("### ", dirname(curdir), fill = TRUE)
  cat(kable(fastq_qc.df[1:2]), fill = TRUE)
  cat(print(kable(fastq_qc.df[c(1,3)])), fill = TRUE)
  cat(print(kable(fastq_qc.df[c(1,4)])), fill = TRUE)
}
```

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

### /sharedspace/its\_and\_18s\_data

| fastq                                    | num\_reads |
|:-----------------------------------------|:-----------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | 16618885   |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | 16618885   |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | 16618885   |

| fastq                                    | first\_read\_id                                      |
|:-----------------------------------------|:-----------------------------------------------------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | M04771:198:000000000-BL3M6:1:1101:15629:1332 1:N:0:0 |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | M04771:198:000000000-BL3M6:1:1101:15629:1332 1:N:0:0 |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | M04771:198:000000000-BL3M6:1:1101:15629:1332 2:N:0:0 |

| fastq                                    | last\_read\_id                                        |
|:-----------------------------------------|:------------------------------------------------------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | M04771:198:000000000-BL3M6:1:2114:15818:29329 1:N:0:0 |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | M04771:198:000000000-BL3M6:1:2114:15818:29329 1:N:0:0 |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | M04771:198:000000000-BL3M6:1:2114:15818:29329 2:N:0:0 |

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): Unequal factor levels: coercing to character

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

    ## Warning in bind_rows_(x, .id): binding character and factor vector,
    ## coercing into character vector

### /sharedspace/its\_and\_18s\_data

| fastq                                    | num\_reads |
|:-----------------------------------------|:-----------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | 13958128   |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | 13958128   |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | 13958128   |

| fastq                                    | first\_read\_id                                      |
|:-----------------------------------------|:-----------------------------------------------------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | M04771:200:000000000-BL3M7:1:1101:16830:1434 1:N:0:0 |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | M04771:200:000000000-BL3M7:1:1101:16830:1434 1:N:0:0 |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | M04771:200:000000000-BL3M7:1:1101:16830:1434 2:N:0:0 |

| fastq                                    | last\_read\_id                                        |
|:-----------------------------------------|:------------------------------------------------------|
| Undetermined\_S0\_L001\_I1\_001.fastq.gz | M04771:200:000000000-BL3M7:1:2114:18865:28936 1:N:0:0 |
| Undetermined\_S0\_L001\_R1\_001.fastq.gz | M04771:200:000000000-BL3M7:1:2114:18865:28936 1:N:0:0 |
| Undetermined\_S0\_L001\_R2\_001.fastq.gz | M04771:200:000000000-BL3M7:1:2114:18865:28936 2:N:0:0 |

Run md5sum
==========

``` bash
cd $RAW_DATA
find -type f -exec md5sum "{}" + > $RAW_DATA/md5sum.txt
cat $RAW_DATA/md5sum.txt
```

    ## d41d8cd98f00b204e9800998ecf8427e  ./md5sum.txt
    ## a2686fc4c1dd8e89d0be327f0a9265fa  ./180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R2_001.fastq.gz
    ## a3a8e448ecf41353487b7f1b9b0a9bcb  ./180223_McCumber_ITS_fastqs/Undetermined_S0_L001_I1_001.fastq.gz
    ## d82e93ba1904173e9eea359fd4a38adf  ./180223_McCumber_ITS_fastqs/Undetermined_S0_L001_R1_001.fastq.gz
    ## c2215bc8a9a8d17bb9afa3d8974df3f9  ./180228_McCumber_18s_fastqs/Undetermined_S0_L001_R2_001.fastq.gz
    ## de109f8e67560f540699bbe6a6fcc0bc  ./180228_McCumber_18s_fastqs/Undetermined_S0_L001_I1_001.fastq.gz
    ## fd0e5b449ddda88d1b254e39a5fb3016  ./180228_McCumber_18s_fastqs/Undetermined_S0_L001_R1_001.fastq.gz

Make the data directory read-only
=================================

``` bash
chmod -R a-w $RAW_DATA
```
