QIIIME2 is installed in the IBIEM environment.

To use it you need to run the command `source activate qiime2-2018.2` to load it before running any qiime2 commands. For example:

``` bash
source activate qiime2-2018.2
qiime --help
```

    ## Usage: qiime [OPTIONS] COMMAND [ARGS]...
    ## 
    ##   QIIME 2 command-line interface (q2cli)
    ##   --------------------------------------
    ## 
    ##   To get help with QIIME 2, visit https://qiime2.org.
    ## 
    ##   To enable tab completion in Bash, run the following command or add it to
    ##   your .bashrc/.bash_profile:
    ## 
    ##       source tab-qiime
    ## 
    ##   To enable tab completion in ZSH, run the following commands or add them to
    ##   your .zshrc:
    ## 
    ##       autoload bashcompinit && bashcompinit && source tab-qiime
    ## 
    ## Options:
    ##   --version  Show the version and exit.
    ##   --help     Show this message and exit.
    ## 
    ## Commands:
    ##   info                Display information about current deployment.
    ##   tools               Tools for working with QIIME 2 files.
    ##   dev                 Utilities for developers and advanced users.
    ##   alignment           Plugin for generating and manipulating alignments.
    ##   composition         Plugin for compositional data analysis.
    ##   cutadapt            Plugin for removing adapter sequences, primers, and
    ##                       other unwanted sequence from sequence data.
    ##   dada2               Plugin for sequence quality control with DADA2.
    ##   deblur              Plugin for sequence quality control with Deblur.
    ##   demux               Plugin for demultiplexing & viewing sequence quality.
    ##   diversity           Plugin for exploring community diversity.
    ##   emperor             Plugin for ordination plotting with Emperor.
    ##   feature-classifier  Plugin for taxonomic classification.
    ##   feature-table       Plugin for working with sample by feature tables.
    ##   gneiss              Plugin for building compositional models.
    ##   longitudinal        Plugin for paired sample and time series analyses.
    ##   metadata            Plugin for working with Metadata.
    ##   phylogeny           Plugin for generating and manipulating phylogenies.
    ##   quality-control     Plugin for quality control of feature and sequence data.
    ##   quality-filter      Plugin for PHRED-based filtering and trimming.
    ##   sample-classifier   Plugin for machine learning prediction of sample
    ##                       metadata.
    ##   taxa                Plugin for working with feature taxonomy annotations.
    ##   vsearch             Plugin for clustering and dereplicating with vsearch.

Unfortunately, because of the way Rmarkdown handles bash chunks, this command needs to be run *once per chunk* in any chunk that uses QIIME2, and it must *precede* and QIIME2 commands.

If you forget to run the `source` command before using qiime2 commands, the chunk will give you an error:

``` bash
qiime --help
```

    ## /tmp/Rtmps2kvxM/chunk-code-3836c1a1a44.txt: line 1: qiime: command not found

``` bash
source activate qiime2-2018.2
qiime --help

printf "\n\nSince the 'source' command works for the whole chunk, I can run the following command without running 'source' again\n\n"

qiime info
```

    ## Usage: qiime [OPTIONS] COMMAND [ARGS]...
    ## 
    ##   QIIME 2 command-line interface (q2cli)
    ##   --------------------------------------
    ## 
    ##   To get help with QIIME 2, visit https://qiime2.org.
    ## 
    ##   To enable tab completion in Bash, run the following command or add it to
    ##   your .bashrc/.bash_profile:
    ## 
    ##       source tab-qiime
    ## 
    ##   To enable tab completion in ZSH, run the following commands or add them to
    ##   your .zshrc:
    ## 
    ##       autoload bashcompinit && bashcompinit && source tab-qiime
    ## 
    ## Options:
    ##   --version  Show the version and exit.
    ##   --help     Show this message and exit.
    ## 
    ## Commands:
    ##   info                Display information about current deployment.
    ##   tools               Tools for working with QIIME 2 files.
    ##   dev                 Utilities for developers and advanced users.
    ##   alignment           Plugin for generating and manipulating alignments.
    ##   composition         Plugin for compositional data analysis.
    ##   cutadapt            Plugin for removing adapter sequences, primers, and
    ##                       other unwanted sequence from sequence data.
    ##   dada2               Plugin for sequence quality control with DADA2.
    ##   deblur              Plugin for sequence quality control with Deblur.
    ##   demux               Plugin for demultiplexing & viewing sequence quality.
    ##   diversity           Plugin for exploring community diversity.
    ##   emperor             Plugin for ordination plotting with Emperor.
    ##   feature-classifier  Plugin for taxonomic classification.
    ##   feature-table       Plugin for working with sample by feature tables.
    ##   gneiss              Plugin for building compositional models.
    ##   longitudinal        Plugin for paired sample and time series analyses.
    ##   metadata            Plugin for working with Metadata.
    ##   phylogeny           Plugin for generating and manipulating phylogenies.
    ##   quality-control     Plugin for quality control of feature and sequence data.
    ##   quality-filter      Plugin for PHRED-based filtering and trimming.
    ##   sample-classifier   Plugin for machine learning prediction of sample
    ##                       metadata.
    ##   taxa                Plugin for working with feature taxonomy annotations.
    ##   vsearch             Plugin for clustering and dereplicating with vsearch.
    ## 
    ## 
    ## Since the 'source' command works for the whole chunk, I can run the following command without running 'source' again
    ## 
    ## System versions
    ## Python version: 3.5.5
    ## QIIME 2 release: 2018.2
    ## QIIME 2 version: 2018.2.0
    ## q2cli version: 2018.2.0
    ## 
    ## Installed plugins
    ## alignment 2018.2.0
    ## composition 2018.2.0
    ## cutadapt 2018.2.0
    ## dada2 2018.2.0
    ## deblur 2018.2.0
    ## demux 2018.2.0
    ## diversity 2018.2.0
    ## emperor 2018.2.0
    ## feature-classifier 2018.2.0
    ## feature-table 2018.2.0
    ## gneiss 2018.2.0
    ## longitudinal 2018.2.0
    ## metadata 2018.2.0
    ## phylogeny 2018.2.0
    ## quality-control 2018.2.0
    ## quality-filter 2018.2.0
    ## sample-classifier 2018.2.0
    ## taxa 2018.2.0
    ## types 2018.2.0
    ## vsearch 2018.2.0
    ## 
    ## Application config directory
    ## /home/guest/.config/q2cli
    ## 
    ## Getting help
    ## To get help with QIIME 2, visit https://qiime2.org
    ## 
    ## Citing QIIME 2
    ## If you use QIIME 2 in any published work, you should cite QIIME 2 and the plugins that you used. To display the citations for QIIME 2 and all installed plugins, run:
    ## 
    ##   qiime info --citations
