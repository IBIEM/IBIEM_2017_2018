-   [QIIME Lecture 1, September 29, 2017](#qiime-lecture-1-september-29-2017)
-   [QIIME Lecture 2, October 6, 2017](#qiime-lecture-2-october-6-2017)
    -   [Generate Subset](#generate-subset)
-   [Metadata Table](#metadata-table)
    -   [Join](#join)
-   [QIIME Lecture 3, October 13, 2017](#qiime-lecture-3-october-13-2017)

QIIME Lecture 1, September 29, 2017
===================================

``` r
data.dir = "/data/ibiem_2016_data/"
out.dir = file.path("~","output","qiime_intro") %>%
  path.expand

subset.dir = file.path(out.dir, "subset_ibiem_2016")

dir.create(out.dir, recursive = TRUE)
dir.create(subset.dir, recursive = TRUE)
```

``` r
list.files(data.dir)
```

``` r
Sys.setenv(OUT_DIR = out.dir)
Sys.setenv(DATA_DIR = data.dir)
Sys.setenv(SUBSET_DIR = subset.dir)
```

``` bash
echo $OUT_DIR
```

QIIME Lecture 2, October 6, 2017
================================

Generate Subset
---------------

``` bash
NUM_READS=10000
RANSEED=1
for FASTQ_FULL in $DATA_DIR/*.gz ; do
  echo $FASTQ_FULL
  FASTQ_BASE=`basename $FASTQ_FULL`
  echo $FASTQ_BASE
  seqtk sample -s $RANSEED $FASTQ_FULL $NUM_READS | gzip -c > $SUBSET_DIR/$FASTQ_BASE
  zcat $SUBSET_DIR/$FASTQ_BASE | wc
done
```

Metadata Table
==============

``` r
library(readr)
map.file = file.path(data.dir, "ibiem_2017_map_v3.txt")
Sys.setenv(MAP_FILE = map.file)

meta.df = read_tsv(map.file)
head(meta.df)
```

``` bash
validate_mapping_file.py -m $MAP_FILE -o $OUT_DIR/validate_mapfile
```

Join
----

``` bash
join_paired_ends.py \
  --forward_reads_fp $SUBSET_DIR/Undetermined_S0_L001_R1_001.fastq.gz \
  --reverse_reads_fp $SUBSET_DIR/Undetermined_S0_L001_R2_001.fastq.gz \
  --index_reads_fp $SUBSET_DIR/Undetermined_S0_L001_I1_001.fastq.gz \
  --output_dir $OUT_DIR/joined
```

``` bash
ls -ltr $OUT_DIR/joined
```

``` bash
wc $OUT_DIR/joined/*
head $OUT_DIR/joined/*
```

QIIME Lecture 3, October 13, 2017
=================================
