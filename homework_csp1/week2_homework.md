# Week 2 Homework (complete by 9/15/2017)
1. Work through the following two lessons and do all the challenges.  Be sure to make notes of questions and problems that you want to go over them in class: 
    * [13. Dataframe Manipulation with dplyr](http://swcarpentry.github.io/r-novice-gapminder/13-dplyr/)
    * [14. Dataframe Manipulation with tidyr](http://swcarpentry.github.io/r-novice-gapminder/14-tidyr/)
2. If you have not already . . . set up a Github account
    1. Go to https://github.com/join to set up an account
        * Be sure to remember your username and password
        * Use a ".edu" email address
        * For *Step2: Choose your personal plan* select "Unlimited public repositories for free."
        * You can skip *Step 3: Tailor your experience*
    2. OPTIONAL: Go to https://education.github.com/ and click on "Request a Discount"
        * Select "Student" and "Individual account"
        * For *How do you plan to use GitHub?*, you can put something like "For a class"
