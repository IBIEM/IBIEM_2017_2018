# Week 1 Homework (complete by 9/8/2017)
1. Complete Challenge 5 in [8. Creating Publication-Quality Graphics](http://swcarpentry.github.io/r-novice-gapminder/08-plot-ggplot2/)
2. Work through the following two lessons and do all the challenges.  Be sure to make notes of questions and problems that you want to go over them in class: 
    * [10. Functions Explained](http://swcarpentry.github.io/r-novice-gapminder/10-functions/)
    * [11. Writing Data](http://swcarpentry.github.io/r-novice-gapminder/11-writing-data/)
3. Set up a Github Educational account
    1. Go to https://github.com/join to set up an account
        * Be sure to remember your username and password
        * Use a ".edu" email address
        * For *Step2: Choose your personal plan* select "Unlimited public repositories for free."
        * You can skip *Step 3: Tailor your experience*
    2. Go to https://education.github.com/ and click on "Request a Discount"
        * Select "Student" and "Individual account"
        * For *How do you plan to use GitHub?*, you can put something like "For a class"
