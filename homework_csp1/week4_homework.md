# Week 4 Homework (complete by 9/29/2017)
0. Create a new project in RStudio (if already created a project to keep notes from bootcamp and/or class, you can use that one.)
1. Enable git for your project
2. Add some files to your git repo: if this was a pre-existing project, add one or more of the pre-existing files, otherwise you can create some dummy files.
3. Create a new repo in your GitHub account for your project
4. Connect your RStudio project repo to your new GitHub repo
5. Synchronize you repos by doing a push, then a pull
6. Look on GitHub to confirm synchronization
