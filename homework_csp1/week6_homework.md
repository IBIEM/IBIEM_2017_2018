# Week 6 Homework (complete by 10/13/2017)
Apply the same steps of the QIIME pipeline that we did in class to the full IBIEM 2016 dataset.

Specifically, you should:

1. Create an Rmarkdown notebook in which you will put all code chunks for analyzing the full dataset

2. Set up appropriate R and Bash variables (e.g. paths that you refer to more than once in your notebook)

3. Check the MD5 sum of the full dataset

4. Validate the mapping file

5. Join the paired end reads


In working on this, you might want to consult the following QIIME related material:

* Markdown versions, which are best for viewing at GitLab
    * [QIIME Intro Notebook](../qiime_intro/qiime_overview.md) 
    * [QIIME Class Notes Notebook](../class_notes/qiime_class_notes.md)
* RMarkdown Notebooks: includes chunk type information that is missing from standard markdown, but isn't rendered on GitLab, so better  viewed in RStudio
    * [QIIME Intro Notebook](../qiime_intro/qiime_overview.Rmd) 
    * [QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd)
