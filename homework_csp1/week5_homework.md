# Week 5 Homework (complete by 10/6/2017)
To prepare for our extensive use of Bash as we work through QIIME, work through the following lessons in the Software Carpentry [Unix Shell Module](http://swcarpentry.github.io/shell-novice/).   The code blocks in these lessons should work normally as **bash** code chunks in an Rmarkdown notebook (with the exception of `nano` as noted below).


1. [Introducing the Shell](http://swcarpentry.github.io/shell-novice/01-intro/)
2. [Navigating Files and Directories](http://swcarpentry.github.io/shell-novice/02-filedir/)
3. [Working With Files and Directories](http://swcarpentry.github.io/shell-novice/03-create/)
  * **Note**: Do not try to use nano instead just open a new "Text File" within RStudio
4. [Pipes and Filters](http://swcarpentry.github.io/shell-novice/04-pipefilter/)
5. [Loops](http://swcarpentry.github.io/shell-novice/05-loop/)
6. [Finding Things](http://swcarpentry.github.io/shell-novice/07-find/)[Finding Things]

Skip the "Shell Scripts" lesson, we will be using Rmarkdown Notebooks instead
