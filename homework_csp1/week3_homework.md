# Week 3 Homework (complete by 9/22/2017)
1. Work through the following two lessons and do all the challenges.  Be sure to make notes of questions and problems that you want to go over them in class: 
    * [15. Producing Reports With knitr](http://swcarpentry.github.io/r-novice-gapminder/15-knitr-markdown/)
    * [16. Writing Good Software](http://swcarpentry.github.io/r-novice-gapminder/16-wrap-up/)

