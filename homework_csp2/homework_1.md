Homework \#1: BIOM file, phylogenetic tree,and phyloseq object
==============================================================

Due: 5pm on January 22, 2018

Assignment
==========

The goal for this homework is to run through the whole QIIME pipeline in preparation for statistical analysis. You will need to: 1. Generate BIOM and tree files for the full lemur dataset 2. Convert the BIOM file to BIOM JSON format 3. Generate Phyloseq Object 4. Check Phyloseq Object

Here are details for each of those steps:

Generate BIOM and tree files for the full lemur dataset
-------------------------------------------------------

In class last semester we walked through the QIIME pipeline, applying it to a subset of the IBIEM 2016 lemur dataset. You were asked to modify this pipeline to run it on the *FULL* dataset. If you haven't done this yet, this will be your first task. In working on this, you might want to consult the following QIIME related material:

-   Markdown versions, which are best for viewing at GitLab
    -   [QIIME Intro Notebook](../qiime_intro/qiime_overview.md)
    -   [QIIME Class Notes Notebook](../class_notes/qiime_class_notes.md)
-   RMarkdown Notebooks: includes chunk type information that is missing from standard markdown, but isn't rendered on GitLab, so better viewed in RStudio
    -   [QIIME Intro Notebook](../qiime_intro/qiime_overview.Rmd)
    -   [QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd)

Convert the BIOM file to BIOM JSON format
-----------------------------------------

For statistical analysis, we will use an excellent R package named phyloseq. There are two different underlying formats for BIOM files: JSON and HDF5. Unfortunately for us, the version of QIIME we are using produces BIOM files in HDF5 format, and the version of phyloseq we are using only reads the JSON format. The good news is that it is relatively easy to convert between the two. The following code chunk will convert the file `seqs_otus.biom` from HDF5 format to JSON format and save it as `seqs_otu.json.biom`

``` bash
set -u

biom convert \
  -i $OUT_DIR/group/seqs_otus.biom \
  -o $OUT_DIR/group/seqs_otu.json.biom \
  --table-type="OTU table" --to-json
```

Generate Phyloseq Object
------------------------

Phyloseq conveniently provides the `import_biom` function for loading a BIOM file. The only thing it requires is the path for the BIOM file, but other data can also be loaded. We will also want to load the phylogenetic tree that we generated (the BIOM format doesn't include trees).

``` r
library(phyloseq)

# Directories
out.dir = file.path("~","output","ibiem2016_fulldata") %>% path.expand
biom_file = file.path(out.dir,"group","seqs_otu.json.biom")
tree_file = file.path(out.dir,"filtered_alignment","rep_set_phylo.tre")

# Load BIOM and tree
full_biom = import_biom(biom_file, treefilename=tree_file)
```

Check Phyloseq Object
---------------------

Finally you should check that everything loaded properly with something like the following

``` r
print(full_biom)
```

Which should return something like this (but with different numbers):

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 10 taxa and 10 samples ]
    ## sample_data() Sample Data:       [ 10 samples by 2 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 10 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 10 tips and 9 internal nodes ]

What you should turn in
-----------------------

1.  Rmarkdown file containing the code you used to generate the BIOM and tree file.
2.  HTML file showing the code and output from the subsequent steps (converting the BIOM file, loading it, and checking it)

### Details

I recommend that you make separate files for parts \#1 and \#2. For part \#1 you can just hand in the Rmarkdown file.

For part \#2, you will need to tell Rmarkdown to show the code chunks. You can do that by putting a chunk like this, at the begining of your R notebook, just below the second `---` in the file that rstudio generates when you tell it you want to make a new R notebook:

    ```{r global_options, include=FALSE}
    knitr::opts_chunk$set(echo=TRUE)
    ```

You can generate an HTML file combining the code and output by selecting the "Knit to HTML" option from the "Knit" menu that is at the top of the source code window in RStudio (sometime this menu will be labelled "Preview" instead of "Knit"). Knitting will run the code chunks, then generate HTML output. I recommend keeping parts \#1 and \#2 separate because knitting part \#2 should only take 1-2 minutes. Running the code in part \#1 will take many hours, we are not requiring knit output from it so you can run it one chunk at a time, debugging as you go.

### Turning In the Assignment

1.  Knit part \#2

2.  In the RStudio file browser, find the HTML file that was produced by Knitting. Click on it and "View in Web Browser" to check that it has all your answers and looks OK.

3.  Follow these directions to download the RMarkdown for part \#1 and the Knit HTML for part \#2: <https://support.rstudio.com/hc/en-us/articles/200713893-Uploading-and-Downloading-Files>

4.  Email the HTML file that you downloaded to Billy <william.gerhard@duke.edu>.
