-   [Get Data](#get-data)
    -   [Check Data Integrity](#check-data-integrity)
    -   [Generate Data Subset for Demo Purposes](#generate-data-subset-for-demo-purposes)
-   [Assemble Metadata Table (Map)](#assemble-metadata-table-map)
    -   [Check Original Map File](#check-original-map-file)
-   [Join](#join)
    -   [Check Join Results](#check-join-results)
-   [Demultiplex](#demultiplex)
-   [Filter](#filter)
    -   [Check for Chimeras](#check-for-chimeras)
        -   [Find Ref](#find-ref)
        -   [Identify Chimeric OTUs](#identify-chimeric-otus)
        -   [Remove Chimeric OTUs](#remove-chimeric-otus)
-   [Group (Count)](#group-count)
-   [Pick Representative Sequences](#pick-representative-sequences)
    -   [Assign Taxonomy](#assign-taxonomy)
-   [Phylogenetic Tree](#phylogenetic-tree)
    -   [Align Sequences](#align-sequences)
    -   [Filter Aligned Sequences](#filter-aligned-sequences)
    -   [Make a Tree](#make-a-tree)
-   [Make a Table](#make-a-table)
    -   [Generate BIOM File](#generate-biom-file)
    -   [Convert BIOM to Table Format](#convert-biom-to-table-format)
    -   [Convert HDF5 BIOM to JSON BIOM](#convert-hdf5-biom-to-json-biom)
-   [Session Info](#session-info)

Get Data
========

We will be starting with a subset of data from last year's IBIEM project

``` r
# Directories
data.dir = "/data/ibiem_2016_data"
out.dir = file.path("~","output","qiime_intro") %>% path.expand
subset.dir = file.path(out.dir, "subset_ibiem_2016")
cache.prefix = file.path(out.dir, "knitr_cache", "qiime_intro__")

# make directory for output
dir.create(out.dir, recursive = TRUE)
dir.create(subset.dir, recursive = TRUE)
# dir.create(cache.path, recursive = TRUE)

# Files
map.file = file.path(data.dir,"ibiem_2017_map_v3.txt")
decoder.file = file.path(data.dir,"ibiem_2017_map_v3_decoder.csv")

# Set variables for bash
Sys.setenv(MAP_FILE = map.file)
Sys.setenv(OUT_DIR = out.dir)
Sys.setenv(DATA_DIR = data.dir)
Sys.setenv(SUBSET_DIR = subset.dir)
```

``` r
list.files(data.dir)
```

    ## [1] "ibiem_2017_map_v1.txt"               
    ## [2] "ibiem_2017_map_v2.txt"               
    ## [3] "ibiem_2017_map_v3_decoder.csv"       
    ## [4] "ibiem_2017_map_v3.txt"               
    ## [5] "md5sum.txt"                          
    ## [6] "Undetermined_S0_L001_I1_001.fastq.gz"
    ## [7] "Undetermined_S0_L001_R1_001.fastq.gz"
    ## [8] "Undetermined_S0_L001_R2_001.fastq.gz"

Check Data Integrity
--------------------

``` bash
cd $DATA_DIR
md5sum -c md5sum.txt
```

    ## ibiem_2017_map_v1.txt: OK
    ## ibiem_2017_map_v2.txt: OK
    ## ibiem_2017_map_v3_decoder.csv: OK
    ## ibiem_2017_map_v3.txt: OK
    ## Undetermined_S0_L001_I1_001.fastq.gz: OK
    ## Undetermined_S0_L001_R1_001.fastq.gz: OK
    ## Undetermined_S0_L001_R2_001.fastq.gz: OK

Generate Data Subset for Demo Purposes
--------------------------------------

``` bash
NUM_READS=10000
RANDSEED=1
for FASTQ_FULL in $DATA_DIR/*.gz ; do
  echo $FASTQ_FULL
  FASTQ_BASE=`basename $FASTQ_FULL`
  echo $FASTQ_BASE
  seqtk sample -s $RANDSEED $FASTQ_FULL $NUM_READS | gzip -c > $SUBSET_DIR/$FASTQ_BASE
  zcat $SUBSET_DIR/$FASTQ_BASE | wc
done
```

    ## /data/ibiem_2016_data/Undetermined_S0_L001_I1_001.fastq.gz
    ## Undetermined_S0_L001_I1_001.fastq.gz
    ##   40000   40000  735064
    ## /data/ibiem_2016_data/Undetermined_S0_L001_R1_001.fastq.gz
    ## Undetermined_S0_L001_R1_001.fastq.gz
    ##   40000   40000 3515064
    ## /data/ibiem_2016_data/Undetermined_S0_L001_R2_001.fastq.gz
    ## Undetermined_S0_L001_R2_001.fastq.gz
    ##   40000   40000 3515064

Assemble Metadata Table (Map)
=============================

You are in luck again, because you have a metadata table already made for you. Let's check it out \#\# Examine Metadata Table (Map)

``` r
library(readr)
```

    ## 
    ## Attaching package: 'readr'

    ## The following object is masked from 'package:rvest':
    ## 
    ##     guess_encoding

``` r
meta.df = read_tsv(map.file)
```

    ## Parsed with column specification:
    ## cols(
    ##   `#SampleID` = col_integer(),
    ##   BarcodeSequence = col_character(),
    ##   LinkerPrimerSequence = col_character(),
    ##   SpeciesName = col_character(),
    ##   AnimalName = col_character(),
    ##   DOB = col_character(),
    ##   CollectionDate = col_character(),
    ##   Age = col_double(),
    ##   ChowType = col_character(),
    ##   ChowWeight = col_double(),
    ##   FruitWeight = col_integer(),
    ##   VegWeight = col_integer(),
    ##   Insects = col_integer(),
    ##   BambooWeight = col_integer(),
    ##   LeavesWeight = col_integer(),
    ##   SocialGroup = col_character(),
    ##   GroupSize = col_integer(),
    ##   Description = col_character()
    ## )

``` r
head(meta.df)
```

    ## # A tibble: 6 x 18
    ##   `#Sam… Barc… Link… Spec… Anim… DOB   Coll…   Age Chow… Chow… Frui… VegW…
    ##    <int> <chr> <chr> <chr> <chr> <chr> <chr> <dbl> <chr> <dbl> <int> <int>
    ## 1      1 CCTC… GTGT… M.mu… Brio… 7/3/… 7/22…  4.05 OWC    1.50    15    15
    ## 2      2 GGCG… GTGT… M.mu… Worm… 5/26… 7/27…  9.18 OWC    1.50    15    15
    ## 3      3 GCGA… GTGT… M.mu… Tzat… 8/15… 7/28…  4.96 OWC    1.50    15    15
    ## 4      4 CAAA… GTGT… M.mu… Wasa… 5/30… 7/29…  8.17 OWC    1.50    15    15
    ## 5      5 TTGT… GTGT… M.mu… Filb… 6/18… 7/29…  3.12 OWC    1.50    15    15
    ## 6      6 CAAT… GTGT… M.mu… Woun… 6/9/… 8/4/…  6.16 OWC    1.50    15    15
    ## # ... with 6 more variables: Insects <int>, BambooWeight <int>,
    ## #   LeavesWeight <int>, SocialGroup <chr>, GroupSize <int>,
    ## #   Description <chr>

Some of those fields are hard to decifer, so lets take a look at our metadata decoder

``` r
cat(read_file(decoder.file))
```

    ## Metadata Info,
    ## ,
    ## Diet Information:,
    ## ,
    ## ChowType,FOL = Folivore; OWC = Old world Chow; G = Gruel
    ## ChowWeight,grams
    ## FruitWeight,grams
    ## VegWeight,grams
    ## Insects,number
    ## BambooWeight,grams
    ## LeavesWeight,grams
    ## ,
    ## Housing Information:,
    ## ,
    ## SocialGroup,Arbitrary number with species intials
    ## GroupSize,Number of individuals in the group at time of collection
    ## ,
    ## *Note: Social group was considered the same if the majority of animals remained the same,

### Check Original Map File

QIIME is inflexible about map file formatting. Fortunately, QIIME includes the [validate\_mapping\_file.py](http://qiime.org/scripts/validate_mapping_file.html) script that checks your map file to see if the format meets its specifications. Unfortunately the script is not very robust, so incorrectly formated map files sometimes make it crash without giving a useful error message. Let's run it anyway . . .

``` bash
validate_mapping_file.py -m $MAP_FILE -o $OUT_DIR/validate_mapfile
```

    ## Errors and/or warnings detected in mapping file.  Please check the log and html file for details.

Once you have run `validate_mapping_file.py` you can view the report through RStudio:

1.  In the *Files* pane, Navigate to /home/guest/output/qiime\_intro/validate\_mapfile
2.  Click on `ibiem_2017_map_v3.html` and select *View in Web Browser*
3.  Look around the output! How does it look?

Join
====

Our data is paired-end, but the "grouping" step, which is at the core of QIIME's functionality, only accepts single-end reads. This means that we have to transform our data into pseudo-single-end reads. We can use the [join\_paired\_ends.py](http://qiime.org/scripts/join_paired_ends.html) script to do this.

``` bash
mkdir -p $OUT_DIR/joined
join_paired_ends.py \
    --forward_reads_fp $SUBSET_DIR/Undetermined_S0_L001_R1_001.fastq.gz \
    --reverse_reads_fp $SUBSET_DIR/Undetermined_S0_L001_R2_001.fastq.gz \
    --index_reads_fp $SUBSET_DIR/Undetermined_S0_L001_I1_001.fastq.gz \
    --output_dir $OUT_DIR/joined
```

Check Join Results
------------------

``` bash
ls $OUT_DIR/joined
```

    ## fastqjoin.join_barcodes.fastq
    ## fastqjoin.join.fastq
    ## fastqjoin.un1.fastq
    ## fastqjoin.un2.fastq

``` bash
wc $OUT_DIR/joined/*
head $OUT_DIR/joined/*
```

    ##   31020   31020  570275 /home/guest/output/qiime_intro/joined/fastqjoin.join_barcodes.fastq
    ##   31020   31020 4304821 /home/guest/output/qiime_intro/joined/fastqjoin.join.fastq
    ##    8980    8980  788899 /home/guest/output/qiime_intro/joined/fastqjoin.un1.fastq
    ##    8980    8980  788899 /home/guest/output/qiime_intro/joined/fastqjoin.un2.fastq
    ##   80000   80000 6452894 total
    ## ==> /home/guest/output/qiime_intro/joined/fastqjoin.join_barcodes.fastq <==
    ## @M04771:88:000000000-AY8YC:1:2105:6615:22291
    ## TGTGGTCATGGC
    ## +
    ## >1111B1B@311
    ## @M04771:88:000000000-AY8YC:1:2104:18441:24033
    ## GTACGTCACTGA
    ## +
    ## A11>>BC11F@D
    ## @M04771:88:000000000-AY8YC:1:1114:11482:4991
    ## CAGCCCTACCCA
    ## 
    ## ==> /home/guest/output/qiime_intro/joined/fastqjoin.join.fastq <==
    ## @M04771:88:000000000-AY8YC:1:2105:6615:22291
    ## TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGACGGGTTTTTAAGTCAGTTGTGAAAGTTCGTGGCTTAACCTTAAAATTGCAGTTGATACTGGAGATCTTGAGTGCAGTTGAGGCAGGCGGAATTCGTGGTGTAGCGGTGAAATGCATAGATATCACGCAGAACTCCAATTGCGAAGGCAGCCTGCTAAGCTGCAACTGACATTGAAGCTCGAAAGTGTGGGGATCAAACAGG
    ## +
    ## AAAAAAAB@FFFGEGGGGGCGGHGCFGGHGHHHGHGGG3FGHHGAEFECGG0FEGGE11BFHHHHHHHF4FGGGBGFGG4?1?EEG0B?GF3B33BFEGHHBGHHHG4GGHHDG<2BFHHHGHHH2/B1BFGFGGGAEFHGFGGHGFGEGHGGGFCHHHHHHGEGF2FDHGHHECE@D1BBFHHBFB1EGGGHHFGEBB0AF1FEBBA01A111GGDFGFD21FFB0EB0GGGGGE1ECGGD1FFFFFAAAAA
    ## @M04771:88:000000000-AY8YC:1:2104:18441:24033
    ## TACGGAGGATCCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGACTATTAAGTCAGCTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAGTTGATACTGGTCGTCTTGAGTGCAGTAGAGGTAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTTACTGGACTGTAACTGACGCTGATGCTCGAAAGTGTGGGTATCAAACAGG
    ## +
    ## BBBBBBBB2FFFG2EEGGGGGGHGGGGGHHHHHHHGGGHHHHHGHGGGGGGGH0E>/>FGHHGH@44F3FHHHHGG3@FFH3/>EEGGHHA?//<?G2GHHHGFFHGDGHHHHH1GFGHHHHHHHHGHHHHFHHHGGGFHHHHGHHGHHHHGGGGGHHGHHHHHHHHHHHHHHHFGEHHHHHGGGGGHGGGGGHHHHHGFHHHHHFGHHHHHHFHHGGGEFFGHHHEGHGGGGGGGGGGGGFFFFFFFBBCCB
    ## @M04771:88:000000000-AY8YC:1:1114:11482:4991
    ## TACGGAGGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGCAGACGGGATGTTAAGTCAGTTGTGAAAGTTTGCGGCTCAACCGTAAAATTGCAATTGATACTGGCGTCCTTGAGTGCGGACGAGGAAGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACCCCGATTGCGAAGGCAGCCTTCCAGGCCGTAACTGACGTTCATGCTCGAAAGTGCGGGTATCAAACAGG
    ## 
    ## ==> /home/guest/output/qiime_intro/joined/fastqjoin.un1.fastq <==
    ## @M04771:88:000000000-AY8YC:1:1107:5493:8729
    ## TACGGAAGGTCCTGGCGTTATCCGGATTTATTGGGTTTAAAGGGTGCGCAGGCGGCTTTTTAAGTCAGCGGTAAAATCGTGTGGCCCAACCGTCAATTGCAGTTGAAAATGTGAGGCCTGAGGGATAGCTGGGGTACCGGAATTCATGGTG
    ## +
    ## AAAA?A1A1B@D1A1GAE?AGGG?CEGGHHHHBGEEFGHBAFGF0BEE/A/AACEG/E0F1/BGGGFHF/>?//@22>//B//?><GEEEE/?///22<1B11B122<?112@D10</@/00///0?111?0>@..11.--<.<0<0<C00
    ## @M04771:88:000000000-AY8YC:1:2112:10399:17680
    ## TACGGAAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGGAGATTAAGCGTGCTGTGAAATGTAGAGGCTCAACCTCTGCACTGCAGCGCGAACTGGTCTTCTTGAGTACGCACAACGTGGGCGGAATTCGTGGT
    ## +
    ## BBBBB@3>2FBFE?FGEGGGGGHGGGGGHGHHHHHGFGHHHHHGFEGGGGGEHCEC//EHDGHH3/1?EFHH?GHHDGGHH4GFEHHGGHHFH3?3FF33F1C1<B/@C@GBB?DGBGGHGEGFFD.<A@CG<-.CECEGGGCEFFGFGG.
    ## @M04771:88:000000000-AY8YC:1:2106:14837:24976
    ## TACGTAGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGAACGGCAAGTTTGAAGTGAAATACCGGGGCTTAACCCGGGAACTGCTTTGAAAACTGTCGATCTAGAGTACCGGAGAGGTAAGTGGAATTCCTGGT
    ## 
    ## ==> /home/guest/output/qiime_intro/joined/fastqjoin.un2.fastq <==
    ## @M04771:88:000000000-AY8YC:1:1107:5493:8729
    ## CCTGTTCGATACCCGCACTTTCGAGCCTCAACGTCAGTCACGCTCCGGACGCCTGCCTTCGCTTTCGGAGTTCTTCATGATATCTGAGCATTTCACCGCTACACCATGAATTCCGCCGACCCTGAGCACGCTCAAGCCTCACAGTATCAAC
    ## +
    ## A3AAAFFABFBBGGAECEEGGGGGHDEGHG3BBCG22F5ADEG?FGG0E000>?BGHHGHHG11FGEF1E??FGHHBFGDGHFHGDFHG3F4FFFFHEE?EEBF2/2?2?FFDFG/?<//@/000011?<<CDAFFF.1/<.<0<<==DDC
    ## @M04771:88:000000000-AY8YC:1:2112:10399:17680
    ## CCTGTTCCATANCCGCACTTTCGAGCCTAAACGTCAGTTCACCTCCGGCAGGCTGCCTTCGCAATCGGGTTTCTGCGTGATATCTATGCATTTCACCGCTACACCTCGCATTCCCCCTTCTTTTCACTCCCTCCACACCCCCCGTTTCAAC
    ## +
    ## >AA1>@33B33#A1AAAEEF1BE00AAFBA1A0BAFAA2221B1BC/AE/A/EFBAEDGH/?//EC?B/>B>1E2B>/E/>F2FHDB2@22B>2F2GG@//>1<B0BF<@<CFF1B//0<111>@2>@10C<@/00?C///--<.</<00G
    ## @M04771:88:000000000-AY8YC:1:2106:14837:24976
    ## CCNGTTTGCTCCCCACGCTTTCGAGCCTCAACGTCAGTTACCGTCCAGTAAGCCGCCTTCGCCACCGGTGTTCTTCCTGATATCTACGCATTTCACCGCTACACCAGGAATTCCACTTACCTCTCCCATACTCAAGATAGACAGTTTCCAA

Demultiplex
===========

Remember from the map file that our dataset consists of reads from multiple samples. In order to tally the number of different bacteria corresponding to each sample, we need to separate reads according to the sample of origin. We can use the [split\_libraries\_fastq.py](http://qiime.org/scripts/split_libraries_fastq.html) script to do this, based on the barcode

``` bash
split_libraries_fastq.py \
    --sequence_read_fps $OUT_DIR/joined/fastqjoin.join.fastq \
    --barcode_read_fps $OUT_DIR/joined/fastqjoin.join_barcodes.fastq \
    --output_dir $OUT_DIR/split \
    --mapping_fps $MAP_FILE \
    --phred_quality_threshold 19 \
    --phred_offset 33 \
    --sequence_max_n 1 \
    --rev_comp_mapping_barcodes \
    --max_barcode_errors 3.5 \
    --barcode_type golay_12

ls $OUT_DIR/split
```

    ## histograms.txt
    ## seqs.fna
    ## split_library_log.txt

``` bash
head $OUT_DIR/split/*
```

    ## ==> /home/guest/output/qiime_intro/split/histograms.txt <==
    ## Length   Count
    ## 209.0    3
    ## 219.0    0
    ## 229.0    0
    ## 239.0    0
    ## 249.0    149
    ## 259.0    0
    ## --
    ## 
    ## 
    ## ==> /home/guest/output/qiime_intro/split/seqs.fna <==
    ## >72_0 M04771:88:000000000-AY8YC:1:1111:7101:5559 orig_bc=AGATTCGCTCGA new_bc=AGATCGGCTCGA bc_diffs=2
    ## TACGGAAGATGCGAGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCGGGCTGTTGAGTCAGCGGTCAAATGTCAGGGCCCAACCTTGGCATGCCGTTGATACTGGCGGCCTTGAATTCACACAAGGAAGGTGGAATTCGTCGTGTAGCGGTGAAATGCTTAGATATGACGAAGAACTCCGATTGCGAAGGCAGCCTTCTGGG
    ## >39_1 M04771:88:000000000-AY8YC:1:2111:20881:28156 orig_bc=CCGCCCATTGCG new_bc=TCCCTCATTGCG bc_diffs=3
    ## TACGGAAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGGAGATTAAGCGTGTTGTGAAATGTAGAGGCTCAACCTCTGCACTGCAGCGCGAACTGGTCTTCTTGAGTACGCACAACGTGGGCGGAATTCGTGGTGTAGCGGTGAAATGCTTAGATATCACGAAGAACTCCGATTGCGAAGGCAGCTCACGGGAGCGCAACTGACGCTGAAGCTCGAAAGTGCGGGTATCGAACAGG
    ## >72_2 M04771:88:000000000-AY8YC:1:1102:24012:8217 orig_bc=AGATTCGCTCGA new_bc=AGATCGGCTCGA bc_diffs=2
    ## TACGGAAGGTCCGGGCGTTATCCGGATTTATTGGGTTTAAAGGGAGCGTAGGCCGCCTTTTAAGCGTGCTGTGAAATACCGTTGCCCAACAACGGGGCTGCAGCGCGAACTGGAGGGCTTGAGTTCACGGGAAGCCGGCGGAACTCGTCGTGTAGCGGTGAAATGCTTAGATATGACGAAGAACTCCGATTGCGAAGGCAGCCGGCTGTAGTGTTACTGACGCTGAAGCTCGAAAGTGCGGGTATCGAACAGG
    ## >72_3 M04771:88:000000000-AY8YC:1:2112:8057:6950 orig_bc=AGATTCGCTCGA new_bc=AGATCGGCTCGA bc_diffs=2
    ## TACGTAGGTTGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGTGTAGGCGGAGAAGCAAGTTGGGAGTGAAATCCATGGGCTCAACCCATGAACTGCTCTCAAAACTGTTTCCCTTGAGTATCGGAGAGGCAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTGCTGGACGACAACTGACGCTGAGGCGCGAAAGCGTGGNGAGCAAACAGG
    ## >22_4 M04771:88:000000000-AY8YC:1:1113:5946:8494 orig_bc=TCGAGCCGATCT new_bc=TCGAGCGAATCT bc_diffs=2
    ## CACGTAAGGCGCGAGCGTTGTTCGGAATCATTGGGCGTAAAGGGCGTGTAGGCGGCCCTGCAAGCCTGGCGTGAAATCCCGGGGCTCAACCCCGGAACCGCGCTGGGAACTGCGAGGCTTGAGCCGCTGTGGCGCAGCCGGAATTCCAGGTGTAGGGGTGAAATCGGTAGATATCTGGAAGAACACCGATGGCGAAGGCAGGCTGCGAGCGG
    ## 
    ## ==> /home/guest/output/qiime_intro/split/split_library_log.txt <==
    ## Input file paths
    ## Mapping filepath: /data/ibiem_2016_data/ibiem_2017_map_v3.txt (md5: 0e8f413f45b6fd1303bda5f401b33013)
    ## Sequence read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join.fastq (md5: 7cfa6c459a908f2d990ce6efcebed827)
    ## Barcode read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join_barcodes.fastq (md5: 89d13da4a0e7a997aefb88fc5c6e5261)
    ## 
    ## Quality filter results
    ## Total number of input sequences: 7755
    ## Barcode not in mapping file: 1083
    ## Read too short after quality truncation: 7
    ## Count of N characters exceeds limit: 0

``` bash
cat $OUT_DIR/split/split_library_log.txt
```

    ## Input file paths
    ## Mapping filepath: /data/ibiem_2016_data/ibiem_2017_map_v3.txt (md5: 0e8f413f45b6fd1303bda5f401b33013)
    ## Sequence read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join.fastq (md5: 7cfa6c459a908f2d990ce6efcebed827)
    ## Barcode read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join_barcodes.fastq (md5: 89d13da4a0e7a997aefb88fc5c6e5261)
    ## 
    ## Quality filter results
    ## Total number of input sequences: 7755
    ## Barcode not in mapping file: 1083
    ## Read too short after quality truncation: 7
    ## Count of N characters exceeds limit: 0
    ## Illumina quality digit = 0: 0
    ## Barcode errors exceed max: 6513
    ## 
    ## Result summary (after quality filtering)
    ## Median sequence length: 253.00
    ## 72   75
    ## 22   58
    ## 10   3
    ## 78   2
    ## 71   2
    ## 92   1
    ## 87   1
    ## 5    1
    ## 40   1
    ## 39   1
    ## 36   1
    ## 29   1
    ## 21   1
    ## 15   1
    ## 117  1
    ## 115  1
    ## 102  1
    ## 99   0
    ## 98   0
    ## 96   0
    ## 94   0
    ## 93   0
    ## 91   0
    ## 90   0
    ## 9    0
    ## 89   0
    ## 88   0
    ## 86   0
    ## 85   0
    ## 84   0
    ## 82   0
    ## 81   0
    ## 80   0
    ## 79   0
    ## 77   0
    ## 76   0
    ## 75   0
    ## 74   0
    ## 73   0
    ## 69   0
    ## 68   0
    ## 67   0
    ## 66   0
    ## 65   0
    ## 64   0
    ## 62   0
    ## 61   0
    ## 60   0
    ## 6    0
    ## 59   0
    ## 58   0
    ## 57   0
    ## 56   0
    ## 55   0
    ## 54   0
    ## 53   0
    ## 52   0
    ## 51   0
    ## 50   0
    ## 49   0
    ## 48   0
    ## 47   0
    ## 46   0
    ## 45   0
    ## 44   0
    ## 43   0
    ## 42   0
    ## 41   0
    ## 4    0
    ## 38   0
    ## 37   0
    ## 35   0
    ## 34   0
    ## 33   0
    ## 32   0
    ## 31   0
    ## 30   0
    ## 3    0
    ## 28   0
    ## 27   0
    ## 26   0
    ## 25   0
    ## 24   0
    ## 23   0
    ## 20   0
    ## 2    0
    ## 19   0
    ## 18   0
    ## 17   0
    ## 16   0
    ## 13   0
    ## 12   0
    ## 116  0
    ## 114  0
    ## 113  0
    ## 112  0
    ## 111  0
    ## 110  0
    ## 11   0
    ## 109  0
    ## 108  0
    ## 107  0
    ## 106  0
    ## 105  0
    ## 104  0
    ## 103  0
    ## 101  0
    ## 100  0
    ## 1    0
    ## 
    ## Total number seqs written    152
    ## ---

Let's take a closer look at this output

`Total number of input sequences: 7755`

So we are inputting 7755 reads into `split_libraries_fastq.py` (Is this different than you expected? Why?)

These are all the problems with the input that cause us to "lose" reads from the output:

    Barcode not in mapping file: 1083
    Read too short after quality truncation: 7
    Barcode errors exceed max: 6513

As a result . . . `Total number seqs written: 152`

We can confirm that 7755 - (1083 + 7 + 6513) = 152

And we can check that the number of reads is 152

``` bash
wc $OUT_DIR/split/seqs.fna 
```

    ##   304   912 54232 /home/guest/output/qiime_intro/split/seqs.fna

So we are losing more than 98% of our reads, almost all because of barcode problems. This could be a problem with our data, but it suggests that there might be a problem with our analysis, particularly how we are handling the barcodes. Perhaps the problem is that the barcodes are reverse complemented relative to the barcodes in the mapping files. Let's drop `--rev_comp_mapping_barcodes` and see if that fixes the problem

``` bash
split_libraries_fastq.py \
    --sequence_read_fps $OUT_DIR/joined/fastqjoin.join.fastq \
    --barcode_read_fps $OUT_DIR/joined/fastqjoin.join_barcodes.fastq \
    --output_dir $OUT_DIR/split \
    --mapping_fps $MAP_FILE \
    --phred_quality_threshold 19 \
    --phred_offset 33 \
    --sequence_max_n 1 \
    --max_barcode_errors 3.5 \
    --barcode_type golay_12

ls $OUT_DIR/split
```

    ## Error in split_libraries_fastq.py: Some or all barcodes are not valid golay codes. Do they need to be reverse complemented? If these are not golay barcodes pass --barcode_type 12 to disable barcode error correction, or pass --barcode_type # if the barcodes are not 12 base pairs, where # is the size of the barcodes. Invalid codes:
    ##  AGGGACTTCAAT AGATTCGCTCGA GCCACCGCCGGA TGGAGCCTTGTC GATCCCACGTAC ACGGCGTTATGT GAACCGTGCAGG AGGGTGACTTTA CTCAGCGGGACG GTTGCTGAGTCC ATTTACAATTGA ATTATGATTATG TCATCTTGATTG CACAACCACAAC TCTTGCGGAGTC ATAATTGCCGAG AGACTTCTCAGG GTGAATGTTCGA TTGCCGCTCTGG TTGTGTCTCCCT GTCCTACACAGC CTATCTCCTGTC ACGTTAATATTC GTCACGGACATT CCAGGACAGGAA CAGCCCTACCCA GAATATACCTGG CCACAACGATCA CAAATTCGGGAT ACGTATTCGAAG TAAGGTCGATAA AGAATCCACCAC CACCATCTCCGG AGTGTTTCGGAC TATATAGTATCC GATATACCAGTG AGTTCTCATTAA TCGCTACAGATG CCGCCAGCTTTG GAAGTAGCGAGC GATGCCTAATGA TTACTTATCCGA TCGTTGGGACTA GTACGTCACTGA GATCTAATCGAG TGACTGCGTTAG AGTGCAGGAGCC CTGATGTACACG TGGCAGCGAGCC CCGCAGCCGCAG ATTTCCGCTAAT ATAGTTAGGGCT GTTAACTTACTA CCTCGCATGACC CCGAGGTATAAT ACACACCCTGAC GTGTCGAGGGCA TTCCACACGTGG CACCCTTGCGAC TTAGTTTGTCAC GCGAGGAAGTCC TATGTTGACGGC CGCAATGAGGGA ACTGTTTACTGT TGGTGGAGTTTC CCACTGCCCACC ATAGTCCTTTAA CGGAGAGACATG CGGCCAGAAGCA TCCGATAATCGG GCGCGGCGTTGC TCGAGCCGATCT AACCGCATAAGT GTCGCTTGCACA TTAACAAGGCAA GGCGTAACGGCA GCCGCCAGGGTC TCTAACGAGTGC GTACTCGAACCA ACACCGCACAAT AATACAGACCTG ATGCCTCGTAAG CGCGCAAGTATT GACGTTAAGAAT GTTGTTCTGGGA CCGGCTTATGTG TCCGCCTAGTCG ATAGGAATAACC AGTTGTAGTCCG CGAATACTGACA TATAGGCTCCGC ATGGGACCTTCA ACGTGCCTTAGA TTCAGTTCGTTA AACTTTCAGGAG GCTGCGTATACC CAAACCTATGGC GGACAAGTGCGA CCGGCCGCGTGC AAGTCACACACA ACTAAGTACCCG GTTTGGCCACAC GACCCGTTTCGC TACGGTCTGGAT CCGCGTCTCAAC AAGGCGCTCCTT GAGCCATCTGTA GAGAAGCTTATA CAATGTAGACAC
    ## 
    ## If you need help with QIIME, see:
    ## http://help.qiime.org
    ## histograms.txt
    ## seqs.fna
    ## seqs.fna.incomplete
    ## split_library_log.txt

``` bash
split_libraries_fastq.py \
    --sequence_read_fps $OUT_DIR/joined/fastqjoin.join.fastq \
    --barcode_read_fps $OUT_DIR/joined/fastqjoin.join_barcodes.fastq \
    --output_dir $OUT_DIR/split \
    --mapping_fps $MAP_FILE \
    --phred_quality_threshold 19 \
    --phred_offset 33 \
    --sequence_max_n 1 \
    --max_barcode_errors 3.5 \
    --barcode_type golay_12 \
    --rev_comp_barcode \
    --rev_comp_mapping_barcodes

head $OUT_DIR/split/split_library_log.txt
tail $OUT_DIR/split/split_library_log.txt
```

    ## Input file paths
    ## Mapping filepath: /data/ibiem_2016_data/ibiem_2017_map_v3.txt (md5: 0e8f413f45b6fd1303bda5f401b33013)
    ## Sequence read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join.fastq (md5: 7cfa6c459a908f2d990ce6efcebed827)
    ## Barcode read filepath: /home/guest/output/qiime_intro/joined/fastqjoin.join_barcodes.fastq (md5: 89d13da4a0e7a997aefb88fc5c6e5261)
    ## 
    ## Quality filter results
    ## Total number of input sequences: 7755
    ## Barcode not in mapping file: 182
    ## Read too short after quality truncation: 334
    ## Count of N characters exceeds limit: 0
    ## 20   46
    ## 41   45
    ## 49   39
    ## 37   38
    ## 96   0
    ## 31   0
    ## 
    ## Total number seqs written    7040
    ## ---

So now more than 90% of reads (7040/7755) are making it through the splitting and filtering process, much better than the original 2%. This seems a lot more reasonable, especially when we consider that most of the excluded reads are due to quality filtering, not barcode problems.

Filter
======

We did some filtering as part of the demultiplexing step, but we have some more to do . . .

Check for Chimeras
------------------

<http://qiime.org/scripts/identify_chimeric_seqs.html>

<http://qiime.org/scripts/align_seqs.html>

<http://qiime.org/tutorials/chimera_checking.html>

<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3044863/>

### Find Ref

``` bash
find /opt/conda/lib/python2.7/site-packages/qiime_default_reference/ -name \*.fasta
```

    ## /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta
    ## /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set_aligned/85_otus.pynast.fasta

### Identify Chimeric OTUs

``` bash
CUR_OUT=$OUT_DIR/usearch_checked_chimeras
time \
identify_chimeric_seqs.py --input_fasta_fp $OUT_DIR/split/seqs.fna \
  --chimera_detection_method usearch61 \
  --reference_seqs_fp /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta \
  --output_fp $CUR_OUT

ls $CUR_OUT
head $CUR_OUT/*.log $CUR_OUT/*.txt
```

    ## 
    ## real 0m26.391s
    ## user 1m12.364s
    ## sys  0m0.816s
    ## chimeras.txt
    ## identify_chimeric_seqs.log
    ## non_chimeras.txt
    ## seqs.fna_chimeras_denovo.log
    ## seqs.fna_chimeras_denovo.uchime
    ## seqs.fna_chimeras_ref.log
    ## seqs.fna_chimeras_ref.uchime
    ## seqs.fna_consensus_fixed.fasta
    ## seqs.fna_consensus_with_abundance.fasta
    ## seqs.fna_consensus_with_abundance.uc
    ## seqs.fna_smallmem_clustered.log
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/identify_chimeric_seqs.log <==
    ## input_seqs_fp    /home/guest/output/qiime_intro/split/seqs.fna
    ## output_dir   /home/guest/output/qiime_intro/usearch_checked_chimeras
    ## reference_seqs_fp    /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta
    ## suppress_usearch61_intermediates False
    ## suppress_usearch61_ref   False
    ## suppress_usearch61_denovo    False
    ## split_by_sampleid    False
    ## non_chimeras_retention   union
    ## usearch61_minh   0.28
    ## usearch61_xn 8.0
    ## 
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_denovo.log <==
    ## vsearch v2.6.0_linux_x86_64, 27.5GB RAM, 4 cores
    ## usearch61 --mindiffs 3 --uchime_denovo /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_consensus_fixed.fasta --minh 0.28 --xn 8.0 --minseqlength 64 --mindiv 0.8 --abskew 2.0 --uchimeout /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_denovo.uchime --dn 1.4 --log /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_denovo.log
    ## Started  Mon Jan 15 20:10:18 2018
    ## 329692 nt in 1308 seqs, min 197, max 287, avg 252
    ## 
    ##     0.28  minh
    ##     8.00  xn
    ##     1.40  dn
    ##     1.00  xa
    ##     0.80  mindiv
    ## 
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_ref.log <==
    ## vsearch v2.6.0_linux_x86_64, 27.5GB RAM, 4 cores
    ## usearch61 --mindiffs 3 --uchime_ref /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_consensus_fixed.fasta --minh 0.28 --xn 8.0 --minseqlength 64 --threads 0.25 --mindiv 0.8 --uchimeout /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_ref.uchime --dn 1.4 --strand plus --db /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta --log /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_chimeras_ref.log
    ## Started  Mon Jan 15 20:10:19 2018
    ## 142290491 nt in 99322 seqs, min 1254, max 2353, avg 1433
    ## 
    ##     0.28  minh
    ##     8.00  xn
    ##     1.40  dn
    ##     1.00  xa
    ##     0.80  mindiv
    ## 
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_smallmem_clustered.log <==
    ## vsearch v2.6.0_linux_x86_64, 27.5GB RAM, 4 cores
    ## usearch61 --maxaccepts 1 --consout /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_consensus_with_abundance.fasta --usersort --id 0.97 --sizeout --minseqlength 64 --wordlength 8 --uc /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_consensus_with_abundance.uc --cluster_smallmem /home/guest/output/qiime_intro/split/seqs.fna --maxrejects 8 --strand plus --log /home/guest/output/qiime_intro/usearch_checked_chimeras/seqs.fna_smallmem_clustered.log
    ## Started  Mon Jan 15 20:10:17 2018
    ## 1775277 nt in 7040 seqs, min 191, max 287, avg 252
    ## 
    ## 
    ##       Alphabet  nt
    ##     Word width  8
    ##      Word ones  8
    ##         Spaced  No
    ## 
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/chimeras.txt <==
    ## 65_1613
    ## 23_4388
    ## 60_5118
    ## 114_2704
    ## 58_5653
    ## 112_1155
    ## 110_3482
    ## 30_4561
    ## 117_2360
    ## 85_2171
    ## 
    ## ==> /home/guest/output/qiime_intro/usearch_checked_chimeras/non_chimeras.txt <==
    ## 74_4384
    ## 105_6591
    ## 74_4387
    ## 104_1661
    ## 18_2359
    ## 62_3072
    ## 47_6896
    ## 28_6105
    ## 110_3489
    ## 56_3152

### Remove Chimeric OTUs

``` bash
CUR_OUT=$OUT_DIR/usearch_checked_chimeras

filter_fasta.py \
  --input_fasta_fp $OUT_DIR/split/seqs.fna \
  --output_fasta_fp $OUT_DIR/usearch_checked_chimeras/seqs_chimeras_filtered.fna \
  --seq_id_fp $OUT_DIR/usearch_checked_chimeras/chimeras.txt \
  --negate

ls -ltr $CUR_OUT
```

    ## total 3824
    ## -rw-r--r-- 1 guest users     967 Jan 15 20:10 seqs.fna_smallmem_clustered.log
    ## -rw-r--r-- 1 guest users  305654 Jan 15 20:10 seqs.fna_consensus_with_abundance.uc
    ## -rw-r--r-- 1 guest users  377978 Jan 15 20:10 seqs.fna_consensus_with_abundance.fasta
    ## -rw-r--r-- 1 guest users  383389 Jan 15 20:10 seqs.fna_consensus_fixed.fasta
    ## -rw-r--r-- 1 guest users  240754 Jan 15 20:10 seqs.fna_chimeras_denovo.uchime
    ## -rw-r--r-- 1 guest users     810 Jan 15 20:10 seqs.fna_chimeras_denovo.log
    ## -rw-r--r-- 1 guest users  132836 Jan 15 20:10 seqs.fna_chimeras_ref.uchime
    ## -rw-r--r-- 1 guest users     929 Jan 15 20:10 seqs.fna_chimeras_ref.log
    ## -rw-r--r-- 1 guest users     700 Jan 15 20:10 identify_chimeric_seqs.log
    ## -rw-r--r-- 1 guest users   52809 Jan 15 20:10 non_chimeras.txt
    ## -rw-r--r-- 1 guest users    3012 Jan 15 20:10 chimeras.txt
    ## -rw-r--r-- 1 guest users 2388219 Jan 15 20:10 seqs_chimeras_filtered.fna

Group (Count)
=============

[OTU picking strategies](http://qiime.org/tutorials/otu_picking.html)

[pick\_open\_reference\_otus.py](http://qiime.org/scripts/pick_open_reference_otus.html)

[pick\_otus.py](http://qiime.org/scripts/pick_otus.html)

``` bash
pick_otus.py \
  --input_seqs_filepath $OUT_DIR/usearch_checked_chimeras/seqs_chimeras_filtered.fna \
  --output_dir $OUT_DIR/group

ls $OUT_DIR/group
head $OUT_DIR/group/seqs*_clusters.uc $OUT_DIR/group/seqs*_otus.log $OUT_DIR/group/seqs*_otus.txt
# head $OUT_DIR/group/*
```

    ## seqs_chimeras_filtered_clusters.uc
    ## seqs_chimeras_filtered_otus.log
    ## seqs_chimeras_filtered_otus.txt
    ## ==> /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_clusters.uc <==
    ## # uclust --input /tmp/UclustExactMatchFilterPZr6PS.fasta --id 0.97 --tmpdir /tmp --w 8 --stepwords 8 --usersort --maxaccepts 1 --stable_sort --maxrejects 8 --uc /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_clusters.uc
    ## # version=1.2.22
    ## # Tab-separated fields:
    ## # 1=Type, 2=ClusterNr, 3=SeqLength or ClusterSize, 4=PctId, 5=Strand, 6=QueryStart, 7=SeedStart, 8=Alignment, 9=QueryLabel, 10=TargetLabel
    ## # Record types (field 1): L=LibSeed, S=NewSeed, H=Hit, R=Reject, D=LibCluster, C=NewCluster, N=NoHit
    ## # For C and D types, PctId is average id with seed.
    ## # QueryStart and SeedStart are zero-based relative to start of sequence.
    ## # If minus strand, SeedStart is relative to reverse-complemented seed.
    ## S    0   253 *   *   *   *   *   QiimeExactMatch.28_110  *
    ## S    1   253 *   *   *   *   *   QiimeExactMatch.71_200  *
    ## 
    ## ==> /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_otus.log <==
    ## UclustOtuPicker parameters:
    ## Application:uclust
    ## Similarity:0.97
    ## enable_rev_strand_matching:False
    ## exact:False
    ## max_accepts:1
    ## max_rejects:8
    ## new_cluster_identifier:denovo
    ## optimal:False
    ## output_dir:/home/guest/output/qiime_intro/group
    ## 
    ## ==> /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_otus.txt <==
    ## denovo0  18_5261
    ## denovo1  34_7021
    ## denovo2  76_3017 87_3117
    ## denovo3  80_3747 16_6053 89_1280 53_3652 55_3903
    ## denovo4  88_6796 60_5002
    ## denovo5  15_4838
    ## denovo6  85_2206
    ## denovo7  82_5098
    ## denovo8  21_2442 80_5495 21_47   16_3691 21_535  28_1010 69_2224 69_1032 21_3155
    ## denovo9  58_87

``` bash
cat $OUT_DIR/group/seqs*_otus.log
```

    ## UclustOtuPicker parameters:
    ## Application:uclust
    ## Similarity:0.97
    ## enable_rev_strand_matching:False
    ## exact:False
    ## max_accepts:1
    ## max_rejects:8
    ## new_cluster_identifier:denovo
    ## optimal:False
    ## output_dir:/home/guest/output/qiime_intro/group
    ## prefilter_identical_sequences:True
    ## presort_by_abundance:True
    ## save_uc_files:True
    ## stable_sort:True
    ## stepwords:8
    ## suppress_sort:True
    ## word_length:8
    ## Num OTUs:939
    ## Result path: /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_otus.txt

Pick Representative Sequences
=============================

[pick\_rep\_set.py](http://qiime.org/scripts/pick_rep_set.html)

``` bash
pick_rep_set.py \
    --input_file $OUT_DIR/group/seqs_chimeras_filtered_otus.txt \
    --fasta_file $OUT_DIR/split/seqs.fna \
    --result_fp $OUT_DIR/group/rep_set.fna

ls $OUT_DIR/group/*
head $OUT_DIR/group/rep_set.fna
```

    ## /home/guest/output/qiime_intro/group/rep_set.fna
    ## /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_clusters.uc
    ## /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_otus.log
    ## /home/guest/output/qiime_intro/group/seqs_chimeras_filtered_otus.txt
    ## >denovo0 18_5261
    ## TACGTAGGTGGCGAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGCGTAGGCGGGCCTTTAAGTCAGATGTGAAAGCCCGGGGCTTAACCCCGGAATTGCATTTGAAACTGAGGGTCTTGAGTGCCGGAGAGGGAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACGGTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAAACAGG
    ## >denovo1 34_7021
    ## TACGTAGGGTGCGAGCGTTAATCGGAATTACTGGGCGTAAAGCGTGCGCAGGCGGTCTGTTAAGACCGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTGGTGACTGGCAGGCTAGAGTATGGCAGAGGGGGGTAGAATTCCACGTGTAGCAGTGAAATGCGTAGATATGTGGAAGAACACCGATGGCGAAGGCAGCCTCCTGGGACATAACTGACGCTCAGGCACGAAAGCGTGGGGAGCAAACAGG
    ## >denovo10 53_35
    ## TACGTAGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCAAGGCAAGTCTGAAGTGAAAACCCAGTGCTCAACGCTGGGATTGCTTTGGAAACTGTTTAGCTGGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAAACAGG
    ## >denovo100 81_4581
    ## TACGTAGGGGGCAAGCGTTATCCGGATTTACTGGTGTAAAGGGAGCGTAGACGGCTGTGCAAGTCTGGTGTGAAAGGCGGGGGCTCAACCCCCGGACTGCATTGGAAACTGTATGGCTTGAGTGCCGGAGAGGTAAGCGGAATTCCTGGTGTAGCGGTGAAATGCGTAGATATCAGGAGGAGCACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAAACAGG
    ## >denovo101 34_4290
    ## TACGAAGGGGGCTAGCGTTGTTCGGAATCACTGGGCGTAAAGGGCGCGTAAGCGGCTTGACAAGTCAGGTGTGAAAGCCCCGGGCTCAACCCGGGAATTGCACTTGATACTGTTTGGCTAGAGTCCGGGAGAGGATGGCGGAATTCCCAGTGTAGAGGTGAAATTCGTAGATATTGGGAAGAACATCGGTGGCGAAGGCGGCCATCTGGACCGGTACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAAACAGG

Assign Taxonomy
---------------

[assign\_taxonomy.py](http://qiime.org/scripts/assign_taxonomy.html)

``` bash
time \
assign_taxonomy.py \
    --input_fasta_fp $OUT_DIR/group/rep_set.fna \
    --output_dir $OUT_DIR/assigned_taxonomy
    
ls $OUT_DIR/assigned_taxonomy
head $OUT_DIR/assigned_taxonomy/rep_set_tax_assignments.txt
```

    ## 
    ## real 0m23.719s
    ## user 0m21.528s
    ## sys  0m2.040s
    ## rep_set_tax_assignments.log
    ## rep_set_tax_assignments.txt
    ## denovo921    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__; g__; s__  1.00    3
    ## denovo920    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__; s__   1.00    3
    ## denovo923    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__; g__; s__  1.00    3
    ## denovo922    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__; g__; s__  1.00    3
    ## denovo925    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__; s__   1.00    3
    ## denovo924    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Lachnospiraceae; g__; s__   1.00    3
    ## denovo927    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Lachnospiraceae; g__; s__   0.67    3
    ## denovo926    k__Bacteria; p__Actinobacteria; c__Actinobacteria; o__Bifidobacteriales; f__Bifidobacteriaceae; g__Bifidobacterium; s__ 0.67    3
    ## denovo929    k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Lachnospiraceae 0.67    3
    ## denovo928    k__Bacteria; p__Bacteroidetes; c__Bacteroidia; o__Bacteroidales; f__Porphyromonadaceae; g__Parabacteroides; s__ 1.00    3

Phylogenetic Tree
=================

1.  [align\_seqs.py](http://qiime.org/scripts/align_seqs.html)
2.  [filter\_alignment.py](http://qiime.org/scripts/filter_alignment.html)
3.  [make\_phylogeny.py](http://qiime.org/scripts/make_phylogeny.html)

Align Sequences
---------------

``` bash
time \
align_seqs.py \
    --input_fasta_fp $OUT_DIR/group/rep_set.fna \
    --output_dir $OUT_DIR/pynast_aligned
    
ls $OUT_DIR/pynast_aligned
# head $OUT_DIR/pynast_aligned/*
# skip
head $OUT_DIR/pynast_aligned/rep_set_failures.fasta $OUT_DIR/pynast_aligned/rep_set_log.txt
head -n2 $OUT_DIR/pynast_aligned/rep_set_aligned.fasta
```

    ## 
    ## real 0m48.331s
    ## user 0m47.496s
    ## sys  0m0.604s
    ## rep_set_aligned.fasta
    ## rep_set_failures.fasta
    ## rep_set_log.txt
    ## ==> /home/guest/output/qiime_intro/pynast_aligned/rep_set_failures.fasta <==
    ## >denovo292 60_21
    ## TCTCTTTAATAACCTGATTCAGCGAAACCAATCCGCGGCATTTAGTAGCGGTAAAGTTAGACCAAACCATGAAACCAACATAAACATTATTGCCCGGCGTACGGGGAAGGACGTCAATAGTCACACAGTCCTTGACGGTATAATAACCACCATCATGGCGACCATCCAAAGGATAAACATCATAGGCAGTCGGGAGGGTAGTCGGAACCGAAGAAGACTCAAAGCGAACCAAACAGGCAAAAAATTTAGGGTCGGCATCAAAAGCAATATCAGCACCAACAGAAACA
    ## >denovo609 42_5636
    ## GCGTTTCTTTGTTCCTGAGCATGGCACTATGTTTACTCTTGCGCTTGTTCGTTTTCCGCCTACTGCGACTAAAGAGATTCAGTACCTTAACGCTAAAGGTGCTTTGACTTATACCGATATTGCTGGCGACCCTGTTTTGTATGGCAACTTGCCGCCGCGTGAAATTTCTATGAAGGATGTTTTCCGTTCTGGTGATTCGTCTAAGAAGTTTAAGATTNCTGAGGGTCAG
    ## 
    ## ==> /home/guest/output/qiime_intro/pynast_aligned/rep_set_log.txt <==
    ## candidate sequence ID    candidate nucleotide count  errors  template ID BLAST percent identity to template  candidate nucleotide count post-NAST
    ## denovo0 18_5261  253     668513  92.90   253
    ## denovo1 34_7021  253     157064  92.90   253
    ## denovo10 53_35   253     216888  95.30   253
    ## denovo100 81_4581    252     193666  94.80   252
    ## denovo101 34_4290    253     569101  90.90   253
    ## denovo102 13_6162    253     159007  96.00   253
    ## denovo103 9_793  253     364903  94.10   253
    ## denovo104 90_1607    210     512191  92.90   210
    ## denovo105 62_3821    253     193666  93.70   253
    ## >denovo0 18_5261 1..253
    ## -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------T--AC---GT-AG-GTG-GCG-A-G-CG-TTGT-C-CGG-AT-TT-A--C-T--GGGT-GTA----AA-GGGC-GC--G-TA-G-G-C-G------------G--G-CC-T-T-T-AA----G-T-C-A---G-ATG-TG-A-AA-GC--CC-GGG-G--------------------------------------------------------------------CT-T-AA-------------------------------------------------------------------------CC-C-C-GG-AA-T----T-G-C-A-T-T--------T--GA-A-A-C-T-G-AGG--G-T-C---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------T-T-G-A-G-T-G-----C-C--GGA-G-A------------G-GG-A-AG-C-----G-GAATT-CCT-A-GT--GT-A-GCG-GTGAAA-TG-CGT-AGAT-A-TT-A-GGA--GG-A-AC-A-CC-AG--T--G--GC-GAA-G--G-C---G----G--C-T-TTCTG------G-AC-GG--------------------------------------------------------------TA-A-C-T--GA--CG-----CT-GA-GG--C-G-CGA--AA-G-C--------------G-TGGG-GAG-C-A-AACA--GG-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Filter Aligned Sequences
------------------------

``` bash
time \
filter_alignment.py \
    --input_fasta_file $OUT_DIR/pynast_aligned/rep_set_aligned.fasta \
    --output_dir $OUT_DIR/filtered_alignment
    
ls $OUT_DIR/filtered_alignment
head $OUT_DIR/filtered_alignment/rep_set_aligned_pfiltered.fasta
```

    ## 
    ## real 0m8.245s
    ## user 0m7.816s
    ## sys  0m0.364s
    ## rep_set_aligned_pfiltered.fasta
    ## >denovo0
    ## ------------------------------------------------------------------TACGTAGGTGGCGAGCGTTGTCCGGATTTACTGGGTGTAAAGGGCGCGTAGGCGGGCCTTTAAGTCAGATGTGAAAGCCCGGGGCTTAACCCCGGAATTGCATTTGAAACTGAGGGTCTTGAGTGCC-GAGAGGGAAGC-GAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAGGAACACCAGTGGCGAAGGCGGCTTTCTGGACGGTAACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAAACAGG-----------------------------------------------------------
    ## >denovo1
    ## ------------------------------------------------------------------TACGTAGGGTGCGAGCGTTAATCGGAATTACTGGGCGTAAAGCGTGCGCAGGCGGTCTGTTAAGACCGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTGGTGACTGGCAGGCTAGAGTATGGCAGAGGGGGGTAGAATTCCACGTGTAGCAGTGAAATGCGTAGATATGTGGAAGAACACCGATGGCGAAGGCAGCCTCCTGGGACATAACTGACGCTCAGGCACGAAAGCGTGGGGAGCAAACAGG-----------------------------------------------------------
    ## >denovo10
    ## ------------------------------------------------------------------TACGTAGGGGGCAAGCGTTATCCGGATTTACTGGGTGTAAAGGGAGCGTAGACGGCAAGGCAAGTCTGAAGTGAAAACCCAGTGCTCAAC-GTGGGATTGCTTTGGAAACTGTTTAGCTGGAGTGCCGGAGAGGTAAGCGGAATTCCTAGTGTAGCGGTGAAATGCGTAGATATTAGGAAGAACACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAAACAGG-----------------------------------------------------------
    ## >denovo100
    ## ------------------------------------------------------------------TACGTAGGGGGCAAGCGTTATCCGGATTTACTGG-TGTAAAGGGAGCGTAGACGGCTGT-CAAGTCTGGTGTGAAAGGCGGGGGCTCAACCCCCGGACTGCATTGGAAACTGTATGGCTTGAGTGCCGGAGAGGTAAGCGGAATTCCTGGTGTAGCGGTGAAATGCGTAGATATCAGGAGGAGCACCAGTGGCGAAGGCGGCTTACTGGACGGTAACTGACGTTGAGGCTCGAAAGCGTGGGGAGCAAACAGG-----------------------------------------------------------
    ## >denovo101
    ## ------------------------------------------------------------------TACGAAGGGGGCTAGCGTTGTTCGGAATCACTGGGCGTAAAGGGCGCGTAAGCGGCTTGACAAGTCAGGTGTGAAAGCCCCGGGCTCAACCCGGGAATTGCACTTGATACTGTTTGGCTAGAGTCCGGGAGAGGATGGCGGAATTCCCAGTGTAGAGGTGAAATTCGTAGATATTGGGAAGAACATCGGTGGCGAAGGCGGCCATCTGGACCGGTACTGACGCTGAGGCGCGAAAGCGTGGGGAGCAAACAGG-----------------------------------------------------------

Make a Tree
-----------

``` bash
time \
make_phylogeny.py \
    --input_fp $OUT_DIR/filtered_alignment/rep_set_aligned_pfiltered.fasta \
    --result_fp $OUT_DIR/filtered_alignment/rep_set_phylo.tre
    
ls $OUT_DIR/filtered_alignment
# head $OUT_DIR/filtered_alignment/rep_set_phylo.tre
head $OUT_DIR/filtered_alignment/*.tre | cut -c1-80
```

    ## 
    ## real 0m10.646s
    ## user 0m10.152s
    ## sys  0m0.428s
    ## rep_set_aligned_pfiltered.fasta
    ## rep_set_phylo.tre
    ## (((((denovo313:0.02893,(denovo650:0.04285,denovo843:0.01925)0.262:0.00933)0.740:

Make a Table
============

[make\_otu\_table.py](http://qiime.org/scripts/make_otu_table.html)

Generate BIOM File
------------------

``` bash
time \
make_otu_table.py \
    --otu_map_fp $OUT_DIR/group/seqs_chimeras_filtered_otus.txt \
    --taxonomy $OUT_DIR/assigned_taxonomy/rep_set_tax_assignments.txt \
    --mapping_fp $MAP_FILE \
    --output_biom_fp $OUT_DIR/group/seqs_otus.biom
    
ls -ltr $OUT_DIR/group
```

    ## 
    ## real 0m1.768s
    ## user 0m1.268s
    ## sys  0m0.440s
    ## total 1028
    ## -rw-r--r-- 1 guest users 320363 Jan 15 20:10 seqs_chimeras_filtered_clusters.uc
    ## -rw-r--r-- 1 guest users  62089 Jan 15 20:10 seqs_chimeras_filtered_otus.txt
    ## -rw-r--r-- 1 guest users    464 Jan 15 20:10 seqs_chimeras_filtered_otus.log
    ## -rw-r--r-- 1 guest users 255194 Jan 15 20:10 rep_set.fna
    ## -rw-r--r-- 1 guest users 399809 Jan 15 20:12 seqs_otus.biom

Convert BIOM to Table Format
----------------------------

``` bash
time \
biom convert -i $OUT_DIR/group/seqs_otus.biom \
    -o $OUT_DIR/group/seqs_otu_table.txt --to-tsv --header-key taxonomy
    
head $OUT_DIR/group/seqs_otu_table.txt
```

    ## 
    ## real 0m0.539s
    ## user 0m0.424s
    ## sys  0m0.108s
    ## # Constructed from biom file
    ## #OTU ID  18  34  76  87  80  16  89  53  55  88  60  15  85  82  21  28  69  58  50  23  46  92  48  52  93  71  45  81  56  91  25  90  78  49  44  22  17  26  57  9   112 104 98  99  11  20  111 101 113 116 103 105 10  110 12  4   117 3   13  106 100 5   115 114 6   109 107 108 77  43  73  51  61  79  64  32  37  33  47  2   1   29  67  42  35  62  59  86  41  84  19  75  68  27  40  24  65  39  94  72  38  30  66  102 36  74  54  taxonomy
    ## denovo0  1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__; s__
    ## denovo1  0.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Proteobacteria; c__Betaproteobacteria; o__Burkholderiales; f__Burkholderiaceae; g__Burkholderia; s__
    ## denovo2  0.0 0.0 1.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__; s__
    ## denovo3  0.0 0.0 0.0 0.0 1.0 1.0 1.0 1.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Lachnospiraceae; g__; s__
    ## denovo4  0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 1.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__; s__
    ## denovo5  0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Lachnospiraceae
    ## denovo6  0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Elusimicrobia; c__Elusimicrobia; o__Elusimicrobiales; f__Elusimicrobiaceae; g__; s__
    ## denovo7  0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 k__Bacteria; p__Firmicutes; c__Clostridia; o__Clostridiales; f__Ruminococcaceae; g__Ruminococcus; s__

Convert HDF5 BIOM to JSON BIOM
------------------------------

``` bash
time \
biom convert -i $OUT_DIR/group/seqs_otus.biom \
    -o $OUT_DIR/group/seqs_otu.json.biom --table-type="OTU table" --to-json
    
head $OUT_DIR/group/seqs_otu.json.biom | cut -c1-200
```

    ## 
    ## real 0m0.525s
    ## user 0m0.412s
    ## sys  0m0.108s
    ## {"id": "No Table ID","format": "Biological Observation Matrix 1.0.0","format_url": "http://biom-format.org","matrix_type": "sparse","generated_by": "BIOM-Format 2.1.5","date": "2018-01-15T20:12:21.154

Session Info
============

``` r
sessionInfo()
```

    ## R version 3.4.1 (2017-06-30)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 16.04.3 LTS
    ## 
    ## Matrix products: default
    ## BLAS: /usr/lib/openblas-base/libblas.so.3
    ## LAPACK: /usr/lib/libopenblasp-r0.2.18.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] datasets  utils     stats     grDevices graphics  methods   base     
    ## 
    ## other attached packages:
    ##  [1] readr_1.1.1        GGally_1.3.2       broom_0.4.3       
    ##  [4] openintro_1.7.1    rvest_0.3.2        xml2_1.1.1        
    ##  [7] stringr_1.2.0      lubridate_1.6.0    googlesheets_0.2.2
    ## [10] ggplot2_2.2.1      rmarkdown_1.8.6    knitr_1.18        
    ## [13] downloader_0.4    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Rcpp_0.12.14       compiler_3.4.1     RColorBrewer_1.1-2
    ##  [4] cellranger_1.1.0   pillar_1.0.1       plyr_1.8.4        
    ##  [7] bindr_0.1          bitops_1.0-6       tools_3.4.1       
    ## [10] digest_0.6.13      lattice_0.20-35    nlme_3.1-131      
    ## [13] evaluate_0.10.1    tibble_1.4.1       gtable_0.2.0      
    ## [16] pkgconfig_2.0.1    rlang_0.1.6        psych_1.7.8       
    ## [19] cli_1.0.0          yaml_2.1.16        parallel_3.4.1    
    ## [22] bindrcpp_0.2       dplyr_0.7.4        httr_1.3.1        
    ## [25] hms_0.3            rprojroot_1.3-2    grid_3.4.1        
    ## [28] reshape_0.8.7      glue_1.2.0         R6_2.2.2          
    ## [31] foreign_0.8-69     reshape2_1.4.3     tidyr_0.7.2       
    ## [34] purrr_0.2.4        magrittr_1.5       backports_1.1.2   
    ## [37] scales_0.5.0       htmltools_0.3.6    mnormt_1.5-5      
    ## [40] assertthat_0.2.0   colorspace_1.3-2   utf8_1.1.3        
    ## [43] stringi_1.1.6      RCurl_1.95-4.8     lazyeval_0.2.0    
    ## [46] munsell_0.4.3      crayon_1.3.4

``` bash
print_qiime_config.py
```

    ## 
    ## System information
    ## ==================
    ##          Platform:   linux2
    ##    Python version:   2.7.13 |Continuum Analytics, Inc.| (default, Dec 20 2016, 23:09:15)  [GCC 4.4.7 20120313 (Red Hat 4.4.7-1)]
    ## Python executable:   /opt/conda/bin/python
    ## 
    ## QIIME default reference information
    ## ===================================
    ## For details on what files are used as QIIME's default references, see here:
    ##  https://github.com/biocore/qiime-default-reference/releases/tag/0.1.3
    ## 
    ## Dependency versions
    ## ===================
    ##           QIIME library version: 1.9.1
    ##            QIIME script version: 1.9.1
    ## qiime-default-reference version: 0.1.3
    ##                   NumPy version: 1.10.4
    ##                   SciPy version: 0.17.1
    ##                  pandas version: 0.18.1
    ##              matplotlib version: 1.4.3
    ##             biom-format version: 2.1.5
    ##                    h5py version: 2.6.0 (HDF5 version: 1.8.16)
    ##                    qcli version: 0.1.1
    ##                    pyqi version: 0.3.2
    ##              scikit-bio version: 0.2.3
    ##                  PyNAST version: 1.2.2
    ##                 Emperor version: 0.9.51
    ##                 burrito version: 0.9.1
    ##        burrito-fillings version: 0.1.1
    ##               sortmerna version: SortMeRNA version 2.0, 29/11/2014
    ##               sumaclust version: SUMACLUST Version 1.0.00
    ##                   swarm version: Swarm 1.2.19 [Mar  1 2016 23:41:10]
    ##                           gdata: Installed.
    ## 
    ## QIIME config values
    ## ===================
    ## For definitions of these settings and to learn how to configure QIIME, see here:
    ##  http://qiime.org/install/qiime_config.html
    ##  http://qiime.org/tutorials/parallel_qiime.html
    ## 
    ##                      blastmat_dir:   None
    ##       pick_otus_reference_seqs_fp:   /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta
    ##                          sc_queue:   all.q
    ##       topiaryexplorer_project_dir:   None
    ##      pynast_template_alignment_fp:   /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set_aligned/85_otus.pynast.fasta
    ##                   cluster_jobs_fp:   start_parallel_jobs.py
    ## pynast_template_alignment_blastdb:   None
    ## assign_taxonomy_reference_seqs_fp:   /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/rep_set/97_otus.fasta
    ##                      torque_queue:   friendlyq
    ##                     jobs_to_start:   1
    ##                        slurm_time:   None
    ##             denoiser_min_per_core:   50
    ## assign_taxonomy_id_to_taxonomy_fp:   /opt/conda/lib/python2.7/site-packages/qiime_default_reference/gg_13_8_otus/taxonomy/97_otu_taxonomy.txt
    ##                          temp_dir:   /tmp/
    ##                      slurm_memory:   None
    ##                       slurm_queue:   None
    ##                       blastall_fp:   blastall
    ##                  seconds_to_sleep:   1
