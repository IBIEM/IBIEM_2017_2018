Relative Abundance and Alpha Diversity Challenges
=================================================

Background
----------

The goal of these challenges is to give you experience with transforming
data, relative abundance plots, pruning taxa, and alpha diversity plots.

Data
----

For all of these challenges we will be using the BIOM file that we
generated in class by generating a subset of the lemur data and running
it through the QIIME pipeline. If you don't have that, just re-run the
[QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd).

Once you load the BIOM file into a phyloseq object, it should look just
like this

    print(subset_biom)

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 937 taxa and 107 samples ]
    ## sample_data() Sample Data:       [ 107 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 937 taxa by 8 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 937 tips and 935 internal nodes ]

Relative Abundance
------------------

### Transform OTU table

Our OTU tables are currently raw counts

    otu_table(subset_biom)[1:10,1:10]

    ## OTU Table:          [10 taxa and 10 samples]
    ##                      taxa are rows
    ##           18 34 76 87 80 16 89 53 55 88
    ## denovo313  0  0  0  0  0  0  0  0  0  0
    ## denovo650  0  0  0  0  0  0  0  0  0  1
    ## denovo843  0  0  0  0  0  0  0  0  0  0
    ## denovo397  0  0  0  0  0  0  0  0  0  0
    ## denovo335  0  0  0  0  1  0  0  0  0  0
    ## denovo302  0  0  0  0  2  0  0  2  0  0
    ## denovo796  0  0  0  0  0  0  0  1  0  0
    ## denovo300  0  0  0  0  0  0  0  1  0  0
    ## denovo561  0  0  0  0  0  0  0  0  0  0
    ## denovo637  1  0  0  0  0  0  0  0  0  0

The first step in making relative abundance plots is to transform your
OTU table from raw counts to relative abundance (also known as
proportional or fractional abundance). Phyloseq provides the
`transform_sample_counts` function for performing user defined
transformations on the OTU table. We want to divide the counts for each
taxa in each sample by the total number of counts for that sample. The
phyloseq object returned has fractional "counts":

    subset_biom.rel  = transform_sample_counts(subset_biom, function(x) x / sum(x) )
    otu_table(subset_biom.rel)[1:10,1:10]

    ## OTU Table:          [10 taxa and 10 samples]
    ##                      taxa are rows
    ##                   18 34 76 87         80 16 89         53 55         88
    ## denovo313 0.00000000  0  0  0 0.00000000  0  0 0.00000000  0 0.00000000
    ## denovo650 0.00000000  0  0  0 0.00000000  0  0 0.00000000  0 0.01282051
    ## denovo843 0.00000000  0  0  0 0.00000000  0  0 0.00000000  0 0.00000000
    ## denovo397 0.00000000  0  0  0 0.00000000  0  0 0.00000000  0 0.00000000
    ## denovo335 0.00000000  0  0  0 0.01851852  0  0 0.00000000  0 0.00000000
    ## denovo302 0.00000000  0  0  0 0.03703704  0  0 0.02631579  0 0.00000000
    ## denovo796 0.00000000  0  0  0 0.00000000  0  0 0.01315789  0 0.00000000
    ## denovo300 0.00000000  0  0  0 0.00000000  0  0 0.01315789  0 0.00000000
    ## denovo561 0.00000000  0  0  0 0.00000000  0  0 0.00000000  0 0.00000000
    ## denovo637 0.01666667  0  0  0 0.00000000  0  0 0.00000000  0 0.00000000

As a result, the sum of all fractional abundance values for each sample
is should be *1*:

    sum(get_taxa(subset_biom.rel, "18"))

    ## [1] 1

### Relative Abundance Plots

#### Challenge 1

Now its your turn! Make a basic relative abundance plot. It should look
like this:

![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/basic_relabund_plot-1.png)

#### Challenge 2

As with the absolute abundance plots, we can color the bars by taxa.
Make Relative abundance plots at the:

1.  Phylum level
2.  Order level
3.  Genus level

They should look like this:

![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/phylum_fill_relabund-1.png)
![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/order_fill_relabund-1.png)
![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/genus_fill_relabund-1.png)

### Pruning Taxa

The problem is that when we do this even at the "Order" level, there are
too many different taxa to be able to make sense of the plot; at the
Genus level it is even worse!

    ntaxa(subset_biom.rel)

    ## [1] 937

    length(get_taxa_unique(subset_biom.rel,"Order"))

    ## [1] 43

    length(get_taxa_unique(subset_biom.rel,"Genus"))

    ## [1] 70

We need to thin things out a bit. There are several strategies to do
this.

#### Average Relative Abundance

We can use the `filter_taxa` function to prune taxa by average
fractional abundance, for example, we can choose to only show taxa that,
on average across all samples, have a relative abundance of at least 1%.

    mean_cutoff = 0.01
    mean_test = function(x) {
      mean(x) >= mean_cutoff
    }
    subset_biom.rel.filt1 = filter_taxa(subset_biom.rel, 
                                        mean_test, 
                                        prune = TRUE)
    ntaxa(subset_biom.rel.filt1)

    ## [1] 14

That `filter_taxa` command can be a little hard to understand, so let's
look at what its actually doing

    filter_taxa

    ## function (physeq, flist, prune = FALSE) 
    ## {
    ##     OTU <- access(physeq, "otu_table", TRUE)
    ##     if (!taxa_are_rows(OTU)) {
    ##         OTU <- t(OTU)
    ##     }
    ##     OTU <- as(OTU, "matrix")
    ##     ans <- apply(OTU, 1, flist)
    ##     if (ntaxa(physeq) != length(ans)) {
    ##         stop("Logic error in applying function(s). Logical result not same length as ntaxa(physeq)")
    ##     }
    ##     if (prune) {
    ##         return(prune_taxa(ans, physeq))
    ##     }
    ##     else {
    ##         return(ans)
    ##     }
    ## }
    ## <environment: namespace:phyloseq>

Let's break it down:

1.  It gets the otu table from `subset_biom.rel` (basically the same as
    `otu_table(physeq)`)
2.  It uses `apply` to run `mean_test` on each row of the otu table.
    `mean_test` calculates the mean of the abundance values in the row,
    then tests if the mean is greater-than-or-equal-to `mean_cutoff`. So
    by running it with `apply`, we get a logical vector with "TRUE" or
    "FALSE" for each taxon indicating whether the mean of abundance
    values for that taxon are greater-than-or-equal-to `mean_cutoff`.
3.  Since we are calling it with `prune = TRUE`, it runs `prune_taxa` on
    `subset_biom.rel` using the results from `mean_test`. If
    `prune=FALSE`, it just returns the results from \#2.

Let's try this by hand. First generate a random matrix

    set.seed(1)
    vals = matrix(runif(30, max = 0.02), nrow=10)
    print(vals)

    ##              [,1]        [,2]         [,3]
    ##  [1,] 0.005310173 0.004119491 0.0186941046
    ##  [2,] 0.007442478 0.003531135 0.0042428504
    ##  [3,] 0.011457067 0.013740457 0.0130334753
    ##  [4,] 0.018164156 0.007682074 0.0025111019
    ##  [5,] 0.004033639 0.015396828 0.0053444134
    ##  [6,] 0.017967794 0.009953985 0.0077222819
    ##  [7,] 0.018893505 0.014352370 0.0002678067
    ##  [8,] 0.013215956 0.019838122 0.0076477591
    ##  [9,] 0.012582281 0.007600704 0.0173938169
    ## [10,] 0.001235725 0.015548904 0.0068069799

Now try running `mean_test` on the first row

    mean_test(vals[1,])

    ## [1] FALSE

Now let's use apply to run `mean_test` row-by-row

    apply(vals, 1, mean_test)

    ##  [1] FALSE FALSE  TRUE FALSE FALSE  TRUE  TRUE  TRUE  TRUE FALSE

Now let's plot relative abundance for the taxa that pass `mean_cutoff`

    plot_bar(subset_biom.rel.filt1, fill="Genus") + 
      geom_bar(aes(color=Genus, fill=Genus), stat="identity", position="stack")

![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/mean_cutoff_relabund_plot-1.png)

In our previous plots, the bars all went to 1.0 relative abundance,
since we were including all taxa. Now that we are pruning some taxa none
of them do, and they are different heights, that's because a lot of the
taxa are excluded from the plot.

> Bonus: You may notice that `ntaxa` tells us that there are 14 taxa
> after filtering, but only 7 show up in the plot. What gives? Try
> looking at `tax_table` to figure out what is going on. It may also
> help to do one of the "default" plots that includes the annoying
> sub-bar outlines.

#### Complex Pruning

We can use more complex rules for pruning taxa.

    min_fraction = 0.05
    min_samples = 3
    subset_biom.rel.filt2 = filter_taxa(subset_biom.rel, 
                           function(x) sum(x >= min_fraction) >= min_samples,
                           prune=TRUE)

    ntaxa(subset_biom.rel.filt2)

    ## [1] 46

Instead of requiring a minimum average across all samples, we can
require that each taxon contstitutes at least 5% (`min_fraction`) of the
counts in at least 3 (`min_samples`) of the samples.

Let's look again at the `filter_taxa` call. Last time we made a function
named `mean_test`, and called `filter_taxa` with `mean_test` as an
argument. Here we are using an anonymous function, in other words
instead of defining a function, giving it a name, and passing that
function name as an argument, we are defining the function on the fly,
which can be more convenient if we are only ever using the function
here, but also more confusing.

Let's break down that function. First we will make our matrix

    set.seed(1)
    vals = matrix(runif(30, max = 0.1), nrow=10)
    print(vals)

    ##              [,1]       [,2]        [,3]
    ##  [1,] 0.026550866 0.02059746 0.093470523
    ##  [2,] 0.037212390 0.01765568 0.021214252
    ##  [3,] 0.057285336 0.06870228 0.065167377
    ##  [4,] 0.090820779 0.03841037 0.012555510
    ##  [5,] 0.020168193 0.07698414 0.026722067
    ##  [6,] 0.089838968 0.04976992 0.038611409
    ##  [7,] 0.094467527 0.07176185 0.001339033
    ##  [8,] 0.066079779 0.09919061 0.038238796
    ##  [9,] 0.062911404 0.03800352 0.086969085
    ## [10,] 0.006178627 0.07774452 0.034034900

Now lets just run the `x >= min_fraction` part on the first row

    vals[1,] >= min_fraction

    ## [1] FALSE FALSE  TRUE

Here we are testing *each value in the row* to see if it is
greater-than-or-equal-to min\_fraction, so for the row, we get a logical
vector back that is the same length. Now let's build on that

    sum(vals[1,] >= min_fraction)

    ## [1] 1

`sum` calculates the sum of the vector. How do you calculate the sum of
a logical vector? Convert it to zeroes and ones (sum does this on the
fly):

    as.numeric(vals[1,] >= min_fraction)

    ## [1] 0 0 1

So the result of `sum(x >= min_fraction)` is just a tally of how many
samples in the row have values greater-than-or-equal-to `min_fraction`

Now its pretty straightforward, just test whether the number of samples
in the row that pass the test is at least `min_samples`

    sum(vals[1,] >= min_fraction) >= min_samples

    ## [1] FALSE

Now let's use apply to run `sum(x >= min_fraction) >= min_samples`
row-by-row

    apply(vals, 1, function(x) sum(x >= min_fraction) >= min_samples)

    ##  [1] FALSE FALSE  TRUE FALSE FALSE FALSE FALSE FALSE FALSE FALSE

Now let's plot relative abundance for the taxa that pass the test

    plot_bar(subset_biom.rel.filt2, fill="Genus") + 
      geom_bar(aes(color=Genus, fill=Genus), stat="identity", position="stack")

![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/min_fraction_relabund_plot-1.png)

#### Count Based Pruning

Sometimes it makes more sense to prune based directly on counts, instead
of relative abundance. For example, we might not want to pay attention
to a taxon that doesn't show up in most samples, and the few samples
that do have it only have 1 or 2 counts.

It is slight more complicated to use counts to prune the relative
abundance data, but phyloseq still makes this pretty easy. So far we
have been using `filter_taxa` with `prune=TRUE`. This returns a new
phyloseq object that has been pruned according to the filtering function
we provide. When `filter_taxa` is called with `prune=FALSE` (the
default), it returns a logical vector describing which taxa pass the
filter function. This boolean vector can then be passed to `prune_taxa`
to generate a pruned phyloseq object. So, we can call `filter_taxa` on
the original *raw count phyloseq* to generate the logical vector, then
call `prune_taxa` with the boolean vector and the *relative abundance
phyloseq* to prune it based on the raw counts.

    min_count = 3
    min_sample_frac = 0.10
    filt_vec3 = filter_taxa(subset_biom, 
                           function(x) sum(x >= min_count) >= (min_sample_frac*length(x)))
    subset_biom.rel.filt3 = prune_taxa(filt_vec3, subset_biom.rel)
    ntaxa(subset_biom.rel.filt3)

    ## [1] 13

Here we are identifying the taxa that have at least 3 (`min_count`)
reads in at least 10% (`min_sample_frac`) of the samples, then using the
logical vector `filt_vec3` to prune `subset_biom.rel`.

    plot_bar(subset_biom.rel.filt3, fill="Genus") + 
      geom_bar(aes(color=Genus, fill=Genus), stat="identity", position="stack")

![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/min_count_relabund_plot-1.png)

#### Challenge 3

Figure out how many taxa have at least 10 reads in at least 4 of the
samples. Then make a relative abundance plot for those taxa.

#### Challenge 4

There is a lot going on in these relative abundance plots, even after
pruning. This makes it hard to see patterns. The data is pretty
heterogeneous, maybe some patterns will fall out if we group by
ChowType. Make a plot that looks like this:
![](rel_abund_diversity_challenge_solutions_files/figure-markdown_strict/chowtype_facet_relabund_plot-1.png)

In order to replicate this plot, you need to prune to include only those
taxa that constitute at least 5% of the reads in at least 2 of the
samples. Then make a relative abundance plot showing samples grouped by
ChowType.

#### Challenge 5

It does look like there might be some patterns, but most of the samples
fall in one group. Identify another categorical variable of interest,
and generate a relative abundance plot similar to the one you just
generated, but grouped by the new categorical variable.

Alpha Diversity Plots
---------------------

### Challenge 6

Make alpha diversity plots using two different measures of alpha
diversity. Choose one measure based on rare taxa (i.e. "weirdos") and
choose a second measure based on surprise (i.e. "gambling"). Group
samples by any categorical variable you like. The [Phyloseq Alpha
diversity
Tutorial](https://joey711.github.io/phyloseq/plot_richness-examples.html)
will be helpful for this challenge.

### Challenge 7

Do you see any groups that show particularly high or low alpha
diversity? A boxplot is a nice way to summarize and compare 1D
scatterplots. Try adding boxplots to your alpha diversity plots.

> Hint: `+ geom_boxplot()`

sessionInfo
===========

It is always a good idea to capture the sessionInfo information so you
know what versions of R and libraries you used!

    sessionInfo()

    ## R version 3.4.1 (2017-06-30)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 16.04.3 LTS
    ## 
    ## Matrix products: default
    ## BLAS: /usr/lib/openblas-base/libblas.so.3
    ## LAPACK: /usr/lib/libopenblasp-r0.2.18.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] datasets  utils     stats     grDevices graphics  methods   base     
    ## 
    ## other attached packages:
    ##  [1] phyloseq_1.22.3    GGally_1.3.2       broom_0.4.3       
    ##  [4] openintro_1.7.1    rvest_0.3.2        xml2_1.1.1        
    ##  [7] stringr_1.2.0      lubridate_1.6.0    googlesheets_0.2.2
    ## [10] ggplot2_2.2.1      rmarkdown_1.8.6    knitr_1.18        
    ## [13] downloader_0.4    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Biobase_2.38.0      httr_1.3.1          tidyr_0.7.2        
    ##  [4] jsonlite_1.5        splines_3.4.1       foreach_1.4.3      
    ##  [7] assertthat_0.2.0    stats4_3.4.1        cellranger_1.1.0   
    ## [10] yaml_2.1.16         pillar_1.0.1        backports_1.1.2    
    ## [13] lattice_0.20-35     glue_1.2.0          digest_0.6.13      
    ## [16] RColorBrewer_1.1-2  XVector_0.18.0      colorspace_1.3-2   
    ## [19] htmltools_0.3.6     Matrix_1.2-10       plyr_1.8.4         
    ## [22] psych_1.7.8         pkgconfig_2.0.1     zlibbioc_1.24.0    
    ## [25] purrr_0.2.4         scales_0.5.0        tibble_1.4.1       
    ## [28] mgcv_1.8-18         IRanges_2.12.0      BiocGenerics_0.24.0
    ## [31] lazyeval_0.2.0      mnormt_1.5-5        survival_2.41-3    
    ## [34] magrittr_1.5        evaluate_0.10.1     nlme_3.1-131       
    ## [37] MASS_7.3-47         foreign_0.8-69      vegan_2.4-5        
    ## [40] tools_3.4.1         data.table_1.10.4-2 S4Vectors_0.16.0   
    ## [43] munsell_0.4.3       cluster_2.0.6       bindrcpp_0.2       
    ## [46] Biostrings_2.46.0   ade4_1.7-10         compiler_3.4.1     
    ## [49] rlang_0.1.6         rhdf5_2.22.0        grid_3.4.1         
    ## [52] RCurl_1.95-4.8      iterators_1.0.8     biomformat_1.6.0   
    ## [55] igraph_1.1.2        bitops_1.0-6        labeling_0.3       
    ## [58] gtable_0.2.0        codetools_0.2-15    multtest_2.34.0    
    ## [61] reshape_0.8.7       reshape2_1.4.3      R6_2.2.2           
    ## [64] dplyr_0.7.4         bindr_0.1           rprojroot_1.3-2    
    ## [67] permute_0.9-4       ape_5.0             stringi_1.1.6      
    ## [70] parallel_3.4.1      Rcpp_0.12.14
