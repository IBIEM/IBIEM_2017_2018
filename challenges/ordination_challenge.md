Ordination Challenges
=====================

Background
----------

The goal of these challenges is to give you experience with ordinations
plots.

Data
----

For all of these challenges we will be using the BIOM file that we
generated in class by generating a subset of the lemur data and running
it through the QIIME pipeline. If you don't have that, just re-run the
[QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd).

References
----------

The [Phyloseq Ordination Plot
Tutorial](https://joey711.github.io/phyloseq/plot_ordination-examples.html)
will be an important reference for these challenges!

Once you load the BIOM file into a phyloseq object, it should look just
like this

    print(subset_biom)

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 937 taxa and 107 samples ]
    ## sample_data() Sample Data:       [ 107 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 937 taxa by 8 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 937 tips and 935 internal nodes ]

Data Preprocessing
------------------

As with relative abundance plots, before performing ordination we will
want to prune rare taxa and transform the data. We prune because we
don't want small differences in rare taxa to swamp out major trends. The
transformation is important because some methods depend on absolute
numerical differences in abundance between samples. Since absolute
counts are not meaningful in amplicon sequence data, we want to prevent
such differences from affecting the ordination results.

### Prune OTUs

As with relative abundance plots, the decision about how to prune is
important, we need to think about what we are throwing away, and how it
might affect the analysis. For starters, we will use the same parameters
as last time - only include taxa that have at least 3 reads in at least
10% of samples

    min_count = 3
    min_sample_frac = 0.10
    filt_vec = filter_taxa(subset_biom, 
                           function(x) sum(x >= min_count) >= (min_sample_frac*length(x)))
    subset_biom.filt = prune_taxa(filt_vec, subset_biom)
    ntaxa(subset_biom.filt)

    ## [1] 13

### Transform to even sampling depth.

Here we are performing the same fractional abundance transformation we
did before, then multiplying by 1 x 10^6 to convert those proprotions
back into whole numbers.

Pay attention to the y-axes in these plots of the raw counts, the pruned
counts, and the transformed counts.

    subset_biom.filt.even = transform_sample_counts(subset_biom.filt, function(x) 1E6 * x/sum(x))
    plot_bar(subset_biom)

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-1.png)

    plot_bar(subset_biom.filt)

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-2.png)

    plot_bar(subset_biom.filt.even)

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-3.png)

Ordination
----------

For ordination plots we have at least two major decisions:

1.  What disimilarity or distance measure will we use?
2.  What ordination method will we use?

For starters, we will use Bray-Curtis to calculate disimilarity between
samples combined with NMDS.

### Challenge 1: NMDS Ordination

Use Bray-Curtis dissimilarity and NMDS to perform ordination on the
filtered, transformed counts.

> Hint: Check out [this
> part](https://joey711.github.io/phyloseq/plot_ordination-examples.html#(1)_just_otus)
> of the phyloseq ordination tutorial. Your results should look
> something like this, although not exactly, since there is some
> randomness invovled.

    ## Square root transformation
    ## Wisconsin double standardization
    ## Run 0 stress 0.1607787 
    ## Run 1 stress 0.1977299 
    ## Run 2 stress 0.182752 
    ## Run 3 stress 0.1566192 
    ## ... New best solution
    ## ... Procrustes: rmse 0.03110059  max resid 0.2663302 
    ## Run 4 stress 0.1692681 
    ## Run 5 stress 0.1632743 
    ## Run 6 stress 0.171114 
    ## Run 7 stress 0.202995 
    ## Run 8 stress 0.1837516 
    ## Run 9 stress 0.1747915 
    ## Run 10 stress 0.1863323 
    ## Run 11 stress 0.1972096 
    ## Run 12 stress 0.1984036 
    ## Run 13 stress 0.1581185 
    ## Run 14 stress 0.1967401 
    ## Run 15 stress 0.1601059 
    ## Run 16 stress 0.1799802 
    ## Run 17 stress 0.1753646 
    ## Run 18 stress 0.1617005 
    ## Run 19 stress 0.1809411 
    ## Run 20 stress 0.170271 
    ## *** No convergence -- monoMDS stopping criteria:
    ##     20: stress ratio > sratmax

Two important things to check are:

1.  Did the NMDS converge?
2.  What is the stress?

<!-- -->

    cat("Converged?", subset_biom.filt.even.nmds_bc$converged, fill=TRUE)

    ## Converged? FALSE

If the NMDS did not converge you can try some of the suggestions in the
*Convergence Problems* section of the help for the NMDS:
`help("metaMDS", "vegan")`. If none of those work, you should just take
the results with a grain of salt, and perhaps try a different ordination
methods.

    cat("Stress:", subset_biom.filt.even.nmds_bc$stress, fill=TRUE)

    ## Stress: 0.1566192

Stress is a measure of how well the NMDS procedure was able to represent
the high dimensional data in the lower dimentionsional space. The stress
is important in understanding how informative the NMDS results are, so
should be presented with the NMDS plot.

> <table>
> <thead>
> <tr class="header">
> <th>Stress Range</th>
> <th>Interpretation</th>
> </tr>
> </thead>
> <tbody>
> <tr class="odd">
> <td>&lt;0.1</td>
> <td>Great</td>
> </tr>
> <tr class="even">
> <td>0.1 - 0.2</td>
> <td>Good</td>
> </tr>
> <tr class="odd">
> <td>0.2 - 0.3</td>
> <td>Acceptable (treat with some caution)</td>
> </tr>
> <tr class="even">
> <td>&gt; 0.3</td>
> <td>Unreliable</td>
> </tr>
> </tbody>
> </table>
>
### Challenge 2: NMDS Plots

Use the results of ordination to generate two NMDS plots where each
datapoint represents a *sample*. The first NMDS plot should color points
by "ChowType", the second should color by "SpeciesName". Your plots
should look very similar to the following.

> Hint: Check out [this
> part](https://joey711.github.io/phyloseq/plot_ordination-examples.html#(2)_just_samples)

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-8-1.png)

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-9-1.png)

Note that changing "color" only changes which metadata is used to
determine color of sample data points, the locations of the points
remains the same

### Challenge 3: Confidence ellipses on NMDS Plots

The large number of samples that are often found in amplicon sequence
projects can make it difficult to visually process ordination plots,
especially if the data is noisy (usually the case). There are a number
of ways to improve the interpretability of ordination plots. These
modifications can be useful, but should be used with care, because
sometimes they make things worse or suggest paterns where none exist.

You can add 95% confidence elipses to ordination plots by appending
`+ stat_ellipse(type = "norm")` after the plotting function. Do this to
make a new NMDS plot that is just like the one you made for Challenge 2,
but with confidence ellipses. Your plot should look like this:

    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-12-1.png)

### Challenge 4: Faceted NMDS Plots

Another option for improving ordination plots is to facet results. Make
an NMDS plot faceted by "SpeciesName". It should look like this:

> Hint: When `facet_grid` is used with a single variable, it puts all
> the facets on the same row. Use a different facet function to make
> your plot look like this one

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-17-1.png)

### Challenge 5: PCoA Plots

There are many other ordination methods supported by phyloseq. Using
what you have already done and the [Phyloseq Ordination Plot
Tutorial](https://joey711.github.io/phyloseq/plot_ordination-examples.html),
make a PCoA plot using Bray-Curtis dissimilarity, coloring the data
points by "SpeciesName". It should look like this:

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-19-1.png)

### Challenge 6: Unifrac

So far we have used the Bray-Curtis dissimilarity to summarize the
differences between samples, but there are many others that we can use.
Another common option is Unifrac, which takes into account the
phylogenetic relationship between taxa observed in the samples. Recall
that there are two versions of Unifrac:

-   Unweighted Unifrac: for each taxon, only takes into account whether
    or not it was observed in a sample
-   Weighted Unifrac: uses the abundance of each taxon in a sample in
    calculating the distance between samples

You should make two more PCoA plots, one using Weighted Unifrac as a
distance metric, and the other using Unweighted Unifrac. Your plots
should look somthing like these:

    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-21-1.png)

    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse
    ## Too few points to calculate an ellipse

![](ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-22-1.png)

sessionInfo
===========

It is always a good idea to capture the sessionInfo information so you
know what versions of R and libraries you used!

    sessionInfo()

    ## R version 3.4.1 (2017-06-30)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 16.04.3 LTS
    ## 
    ## Matrix products: default
    ## BLAS: /usr/lib/openblas-base/libblas.so.3
    ## LAPACK: /usr/lib/libopenblasp-r0.2.18.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] datasets  utils     stats     grDevices graphics  methods   base     
    ## 
    ## other attached packages:
    ##  [1] vegan_2.4-5        lattice_0.20-35    permute_0.9-4     
    ##  [4] dplyr_0.7.4        phyloseq_1.22.3    GGally_1.3.2      
    ##  [7] broom_0.4.3        openintro_1.7.1    rvest_0.3.2       
    ## [10] xml2_1.1.1         stringr_1.2.0      lubridate_1.6.0   
    ## [13] googlesheets_0.2.2 ggplot2_2.2.1      rmarkdown_1.8.6   
    ## [16] knitr_1.18         downloader_0.4    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Biobase_2.38.0      httr_1.3.1          tidyr_0.7.2        
    ##  [4] jsonlite_1.5        splines_3.4.1       foreach_1.4.3      
    ##  [7] assertthat_0.2.0    stats4_3.4.1        cellranger_1.1.0   
    ## [10] yaml_2.1.16         pillar_1.0.1        backports_1.1.2    
    ## [13] glue_1.2.0          digest_0.6.13       RColorBrewer_1.1-2 
    ## [16] XVector_0.18.0      colorspace_1.3-2    htmltools_0.3.6    
    ## [19] Matrix_1.2-10       plyr_1.8.4          psych_1.7.8        
    ## [22] pkgconfig_2.0.1     zlibbioc_1.24.0     purrr_0.2.4        
    ## [25] scales_0.5.0        tibble_1.4.1        mgcv_1.8-18        
    ## [28] IRanges_2.12.0      BiocGenerics_0.24.0 lazyeval_0.2.0     
    ## [31] mnormt_1.5-5        survival_2.41-3     magrittr_1.5       
    ## [34] evaluate_0.10.1     nlme_3.1-131        MASS_7.3-47        
    ## [37] foreign_0.8-69      tools_3.4.1         data.table_1.10.4-2
    ## [40] S4Vectors_0.16.0    munsell_0.4.3       cluster_2.0.6      
    ## [43] bindrcpp_0.2        Biostrings_2.46.0   ade4_1.7-10        
    ## [46] compiler_3.4.1      rlang_0.1.6         rhdf5_2.22.0       
    ## [49] grid_3.4.1          RCurl_1.95-4.8      iterators_1.0.8    
    ## [52] biomformat_1.6.0    igraph_1.1.2        bitops_1.0-6       
    ## [55] labeling_0.3        gtable_0.2.0        codetools_0.2-15   
    ## [58] multtest_2.34.0     reshape_0.8.7       reshape2_1.4.3     
    ## [61] R6_2.2.2            bindr_0.1           rprojroot_1.3-2    
    ## [64] ape_5.0             stringi_1.1.6       parallel_3.4.1     
    ## [67] Rcpp_0.12.14
