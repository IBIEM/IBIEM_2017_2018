Abundance Plot Challenges
=========================

Background
----------

The goal of these challenges are to give you experience with absolute abundance plots, as well as with several features of phyloseq. For several technical reasons, the absolute abundance of amplicon sequencing reads is not biologically meaningful. However, absolute abundance is important for quality control.

Resources
---------

These challenges are based on the [phyloseq plot\_bar tutorial](https://joey711.github.io/phyloseq/plot_bar-examples.html), which will give you starting points for most of the challenges.

Data
----

For all of these challenges we will be using the BIOM file that we generated in class by generating a subset of the lemur data and running it through the QIIME pipeline. If you don't have that, just re-run the [QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd).

Once you load the BIOM file into a phyloseq object, it should look just like this

``` r
print(subset_biom)
```

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 937 taxa and 107 samples ]
    ## sample_data() Sample Data:       [ 107 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 937 taxa by 7 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 937 tips and 935 internal nodes ]

Basic Challenges
----------------

### Simple absolute abundance plot

Start by making a simple absolute abundance bar plot with samples on the x-axis and absolute abundance on the y-axis. Here is an example from the phyloseq tutorial. ![basic abundance plot](https://joey711.github.io/phyloseq/plot_bar-examples_files/figure-html/unnamed-chunk-3-1.png)

### Absolute abundance plot with kingdom

Now let's look add the kingdom distribution of the reads. This is a good sanity check for contamination - we expect the *vast* majority of our reads to be bacterial. There is an example from the phyloseq tutorial doing this at the *genus* level. ![abundance plot with phylogeny](https://joey711.github.io/phyloseq/plot_bar-examples_files/figure-html/unnamed-chunk-4-1.png)

### Absolute abundance plot with phyllum

Now do the same as above, but with coloring by phyllum instead of by kingdom.

Intermediate Challenges
-----------------------

### Facet Plots

From our plots so far, we see some variation in number of reads per sample. This is to be expected, but we want to be sure that there are no confounding problems, for example, categories of samples that are outliers. To this end, we can make facet plots to group samples by different metadata features. Try to remake the kingdom absolute abundance plot so that samples are grouped by ChowType, this will give us an idea if there is a correlation between chow type and abundance. This plot from the phyloseq tutorial should give you an idea of what you are aiming for: ![abundance plot with phylogeny](https://joey711.github.io/phyloseq/plot_bar-examples_files/figure-html/unnamed-chunk-6-1.png)

Depending on how you do this, you might notice some weird things about your plot: 1. The bars are all black 2. There are spaces between samples

The next challenge will address \#1 and we will address \#2 in the *Advanced Challeges* section

### Getting rid of annoying black lines

You may have noticed that the bars are divided by short horizontal black lines. This is because each bar actually consists of a stack of sub-bars, each representing a different OTU. Usually the sub-bars delineation are more confusing than helpful, so I prefer to get rid of it. This isn't that hard to do! by default `plot_bar` outlines each sub-bar in black, but if we outline sub-bars with the same color as the bar fill, we get rid of the delineation. Try re-making the *Absolute abundance plot with kingdom* from above, but without the black sub-bar outlines. The section [Add ggplot2 layer to remove the OTU separation lines](https://joey711.github.io/phyloseq/plot_bar-examples.html#add-ggplot2-layer-to-remove-the-otu-separation-lines) should help you do this

### Facet Plot without annoying black lines

Now try to make the "ChowType" facet plot without the bar outlines.

Advanced Challenges
-------------------

### Missing Values in Facet Plots

By default, the facets of a facet plot use the same scale. Often this is the right thing to do - if the y-axis of each of the abundance plots had a different scale, it would be hard to compare bars between the facets. But sometimes it is not, such as our case. For the ChowType facet plot, since the x-axis is samples, each sample has a place in each facet, even though ChowType is mutually exclusive - each sample corresponds to a lemur, and each lemur only gets one ChowType. Try to fix the ChowType facet plot so that each facet only shows the samples that correspond to that ChowType.

Once you fix that, you will notice that the facets default to being the same size, despite the number of bars in the facet. Try adjusting the facet plot so the bars are all the same width, by making the size of the facet proportional to the number bars in it. Hint: check out the `space` argument to `facet_grid`

### Abundance Plots without plot\_bar

`plot_bar` is pretty convenient, but sometimes you might want more control over plots. One way is to add ggplot2 layers, as we did above, another is to bypass `plot_bar` and use ggplot2 directly. Try making one of the above bar plots without `plot_bar`

> Hint: you can see the code for an R function by calling the function without parentheses or arguments

### Correct taxonomic levels

If you have gotten this far, you have noticed that the biom files uses Rank1, Rank2, etc to refer to taxonomic levels. This can be annoying, but it is fixable. Try to correct the phyloseq object, replacing "Rank1, Rank2, etc" with the correct names for the taxonomic levels (Kingdom,Phyllum, Class,Order,Family,Genus,Species), then remake the *Absolute abundance plot with kingdom*, refering to the taxonomic level correctly (i.e. "Kingdom").

> Hint: Look at the `tax_table` method for phyloseq objects.
