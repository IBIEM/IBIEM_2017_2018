Permanova Challenges
====================

Background
----------

The goal of these challenges is to give you experience with permanova
analysis.

Data
----

For all of these challenges we will be using the BIOM file that we
generated in class by generating a subset of the lemur data and running
it through the QIIME pipeline. If you don't have that, just re-run the
[QIIME Class Notes Notebook](../class_notes/qiime_class_notes.Rmd).

References
----------

The [Permanova
Section](http://deneflab.github.io/MicrobeMiseq/demos/mothur_2_phyloseq.html#permanova)
from the "Microbial Community Diversity Analysis Tutorial with Phyloseq"
will be an important reference for these challenges!
<!-- https://rpubs.com/dillmcfarlan/R_microbiotaSOP -->

Once you load the BIOM file into a phyloseq object, it should look just
like this

    print(subset_biom)

    ## phyloseq-class experiment-level object
    ## otu_table()   OTU Table:         [ 937 taxa and 107 samples ]
    ## sample_data() Sample Data:       [ 107 samples by 17 sample variables ]
    ## tax_table()   Taxonomy Table:    [ 937 taxa by 8 taxonomic ranks ]
    ## phy_tree()    Phylogenetic Tree: [ 937 tips and 935 internal nodes ]

Data Preprocessing
------------------

As with relative abundance plots, before performing ordination we will
want to prune rare taxa and transform the data. We prune because we
don't want small differences in rare taxa to swamp out major trends. The
transformation is important because some methods depend on absolute
numerical differences in abundance between samples. Since absolute
counts are not meaningful in amplicon sequence data, we want to prevent
such differences from affecting the ordination results.

### Prune OTUs

As with relative abundance plots, the decision about how to prune is
important, we need to think about what we are throwing away, and how it
might affect the analysis. For starters, we will use the same parameters
as last time - only include taxa that have at least 3 reads in at least
10% of samples

    min_count = 3
    min_sample_frac = 0.10
    filt_vec = filter_taxa(subset_biom, 
                           function(x) sum(x >= min_count) >= (min_sample_frac*length(x)))
    subset_biom.filt = prune_taxa(filt_vec, subset_biom)
    ntaxa(subset_biom.filt)

    ## [1] 13

### Transform to even sampling depth.

Here we are performing the same fractional abundance transformation we
did before, then multiplying by 1 x 10^6 to convert those proprotions
back into whole numbers.

Pay attention to the y-axes in these plots of the raw counts, the pruned
counts, and the transformed counts.

    subset_biom.filt.even = transform_sample_counts(subset_biom.filt, function(x) 1E6 * x/sum(x))
    plot_bar(subset_biom)

![](permanova_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-1.png)

    plot_bar(subset_biom.filt)

![](permanova_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-2.png)

    plot_bar(subset_biom.filt.even)

![](permanova_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-3-3.png)

Permanova
---------

As with NMDS, we need to transform the data to an even sampling depth,
and it is probably a good idea to prune rare taxa.

For permanova we have to decide what disimilarity or distance measure
will we use. For starters, we will use Bray-Curtis to calculate
disimilarity between samples.

### Generate Bray-Curtis Disimilarity Matrix

    subset.bc <- phyloseq::distance(subset_biom.filt.even, "bray")

### Make a data frame from the metadata

    subset.samdat = data.frame(sample_data(subset_biom.filt.even))

### Run Permanova

    # since permanova depends on randome permutations, we want to set the random 
    # seed so that we get the same result if we run the same analysis in the future
    set.seed(1)

    subset.perman = adonis(subset.bc ~ ChowType, data = subset.samdat)
    print(subset.perman)

    ## 
    ## Call:
    ## adonis(formula = subset.bc ~ ChowType, data = subset.samdat) 
    ## 
    ## Permutation: free
    ## Number of permutations: 999
    ## 
    ## Terms added sequentially (first to last)
    ## 
    ##            Df SumsOfSqs MeanSqs F.Model      R2 Pr(>F)  
    ## ChowType    2     1.337 0.66842  2.0826 0.03851  0.011 *
    ## Residuals 104    33.379 0.32095         0.96149         
    ## Total     106    34.716                 1.00000         
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

The result is mildly significant, with a p-value of 0.011. Since
PERMANOVA is sensitive to differences in variance between samples, we
need to run a test of for homogeneity of dispersion.

### Homogeneity of dispersion test

    # since the test for Homogeneity of dispersion depends on randome permutations, 
    # we want to set the random seed so that we get the same result if we run the 
    # same analysis in the future
    set.seed(1) 

    subset.bc.beta = betadisper(subset.bc, subset.samdat$ChowType)
    subset.bc.betapermute = permutest(subset.bc.beta)
    print(subset.bc.betapermute)

    ## 
    ## Permutation test for homogeneity of multivariate dispersions
    ## Permutation: free
    ## Number of permutations: 999
    ## 
    ## Response: Distances
    ##            Df  Sum Sq  Mean Sq      F N.Perm Pr(>F)   
    ## Groups      2 0.15652 0.078258 8.8885    999  0.009 **
    ## Residuals 104 0.91566 0.008804                        
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Unfortunately, the test for Homogeneity of dispersion also yields a
signnifcant result, with a p-value of 0.009, which means that our
permanova result is suspect, and could have been caused by differences
in dispersion between the ChowType groups, and not differences in
locations of the group centroid.

This is not too suprising if we look back at our NMDS Plot of ChowType,
by eyeball the dispersion seems different. ![ChowType NMDS
Plot](https://gitlab.oit.duke.edu/IBIEM/IBIEM_2017_2018/raw/master/challenges/ordination_challenge_solutions_files/figure-markdown_strict/unnamed-chunk-8-1.png)

This is certainly exacerbated by the large difference in sample size
between the groups:

    table(subset.samdat$ChowType)

    ## 
    ## FOL   G OWC 
    ##   5   3  99

Challenge 1: Permanova
----------------------

Use Bray-Curtis dissimilarity to perform permanova on *SpeciesName*.

Does the test support a hypothesis that there is a significant
difference in microbial communities between species?

Challenge 2: Homogeneity of Dispersion Test
-------------------------------------------

Run a homogeneity of dispersion test to investigate whether variance may
be affecting the permanova results.

Does the result of the homogeneity of dispersion test change your
interpretation of the permanova? How?

sessionInfo
===========

It is always a good idea to capture the sessionInfo information so you
know what versions of R and libraries you used!

    sessionInfo()

    ## R version 3.4.1 (2017-06-30)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Ubuntu 16.04.3 LTS
    ## 
    ## Matrix products: default
    ## BLAS: /usr/lib/openblas-base/libblas.so.3
    ## LAPACK: /usr/lib/libopenblasp-r0.2.18.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] datasets  utils     stats     grDevices graphics  methods   base     
    ## 
    ## other attached packages:
    ##  [1] vegan_2.4-6        lattice_0.20-35    permute_0.9-4     
    ##  [4] dplyr_0.7.4        phyloseq_1.22.3    GGally_1.3.2      
    ##  [7] broom_0.4.3        openintro_1.7.1    rvest_0.3.2       
    ## [10] xml2_1.1.1         stringr_1.2.0      lubridate_1.6.0   
    ## [13] googlesheets_0.2.2 ggplot2_2.2.1      rmarkdown_1.8.6   
    ## [16] knitr_1.18         downloader_0.4    
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] Biobase_2.38.0      httr_1.3.1          tidyr_0.7.2        
    ##  [4] jsonlite_1.5        splines_3.4.1       foreach_1.4.3      
    ##  [7] assertthat_0.2.0    stats4_3.4.1        cellranger_1.1.0   
    ## [10] yaml_2.1.16         pillar_1.0.1        backports_1.1.2    
    ## [13] glue_1.2.0          digest_0.6.13       RColorBrewer_1.1-2 
    ## [16] XVector_0.18.0      colorspace_1.3-2    htmltools_0.3.6    
    ## [19] Matrix_1.2-10       plyr_1.8.4          psych_1.7.8        
    ## [22] pkgconfig_2.0.1     zlibbioc_1.24.0     purrr_0.2.4        
    ## [25] scales_0.5.0        tibble_1.4.1        mgcv_1.8-18        
    ## [28] IRanges_2.12.0      BiocGenerics_0.24.0 lazyeval_0.2.0     
    ## [31] mnormt_1.5-5        survival_2.41-3     magrittr_1.5       
    ## [34] evaluate_0.10.1     nlme_3.1-131        MASS_7.3-47        
    ## [37] foreign_0.8-69      tools_3.4.1         data.table_1.10.4-2
    ## [40] S4Vectors_0.16.0    munsell_0.4.3       cluster_2.0.6      
    ## [43] bindrcpp_0.2        Biostrings_2.46.0   ade4_1.7-10        
    ## [46] compiler_3.4.1      rlang_0.1.6         rhdf5_2.22.0       
    ## [49] grid_3.4.1          RCurl_1.95-4.8      iterators_1.0.8    
    ## [52] biomformat_1.6.0    igraph_1.1.2        bitops_1.0-6       
    ## [55] labeling_0.3        gtable_0.2.0        codetools_0.2-15   
    ## [58] multtest_2.34.0     reshape_0.8.7       reshape2_1.4.3     
    ## [61] R6_2.2.2            bindr_0.1           rprojroot_1.3-2    
    ## [64] ape_5.0             stringi_1.1.6       parallel_3.4.1     
    ## [67] Rcpp_0.12.14
