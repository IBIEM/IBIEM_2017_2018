Week 2 Challenges
=================

Functions
---------

### Beginner

Using the gapminder dataset, write a function that creates a data frame of countries that have a population greater than a user-specified minimum

### Intermediate

Write a function that does the same as above, but has a reasonable default value for the minimum number.

### Advanced

Generalize the function above to use any user-specified feature variable (i.e. have feature variable as a function input).

Writing Data
------------

### Beginner

1.  Write the first 20 lines of gapminder to a CSV file

2.  Write the first 20 lines of gapminder to a tab delimited file

### Intermediate

1.  Write the first 20 lines of gapminder to a semi-colon delimited file.

2.  Write a function that writes the first 20 lines of a dataframe to a CSV file. The dataframe and output file name should be supplied as arguments to the function.
