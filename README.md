# IBIEM Information
Shortened URL for this page: http://bit.ly/2wNtaeh

## Challenges
* [Absolute Abundance Plot Challenges](challenges/abundance_plot_challenge.md)
* [Relative Abundance Plot and Alpha Diversity Challenges](challenges/rel_abund_diversity_challenge.md)
* [Ordination Challenges](challenges/ordination_challenge.md)
* [Permanova Challenges](challenges/permanova_challenge.md)

## Homework
* [Homework #1](homework_csp2/homework_1.md)

## Lessons
* [R for Reproducible Scientific Analysis](http://swcarpentry.github.io/r-novice-gapminder/)
* [Git in RStudio](git_material/git_overview.md)
* [QIIME Intro](qiime_intro/qiime_overview.md)

## Misc
* [Accessing IBIEM Computing Environment](misc/accessing_computing_environment.md)
* [Pull Course Repo](git_material/pulling_course_repo.md)
* [Long Running Chunks in RStudio](misc/long_running_jobs.md)
* [Troubleshooting](misc/troubleshooting.md)

### Project Related Material
* [Project Computational Issues](misc/project_computational_issues.md)
* [Using QIIME2](misc/using_qiime2.md)

#### Loading Data into IBIEM Computing Environment
It can be a bit tricky to move data into the computing environment, because of the way it is set up.  Here are several options listed roughly in order from least to most complicated:

1. If you want to transfer a file that is on your local computer, and it is less than 100 MB, you can follow these instructions for  [Uploading Files](https://support.rstudio.com/hc/en-us/articles/200713893-Uploading-and-Downloading-Files).

2. If the file is on a public web or ftp server, you can follow the instructions for [Downloading Data From Argonne National Labs](misc/argonne_download.md).  If you are downloading from someplace other than Argonne, there will be some subtle differences, but the basic idea is the same.

3. Follow [Download Data from SRA](misc/sra_download.md) if the file is available at SRA (i.e. published data).

4. Follow [Download Data from Dropbox](misc/dropbox_download.md) if the file is on dropbox (or you can put it on dropbox)

5. If none of the above fit the bill you will need to pull the file from a server that supports rsync, sftp, or scp using the Rstudio terminal.


#### Download References into IBIEM Computing Environment
* [Download Dada2 References](misc/download_dada_references.md)

