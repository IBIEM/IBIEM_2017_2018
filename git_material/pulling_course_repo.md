# Pulling Course Repo

If you have not already, you will need to follow the instructions to [Setup Git in Rstudio](https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN)

The following steps will allow you to easily download the course Git repository as an RStudio project [^1] :
1. Execute the New Project command (from the Project menu)
2. Choose to create a new project from Version Control
3. Choose Git
4. Fill in the *Repository URL*: https://gitlab.oit.duke.edu/IBIEM/IBIEM_2017_2018.git
5. Once the *Repository URL* is filled in, *Project directory name* should automatically fill in as IBIEM_2017_2018
6. Leave *Create project as subdirectory of* as "~"
7. Click Create Project


[^1]: Modified from https://support.rstudio.com/hc/en-us/articles/200532077-Version-Control-with-Git-and-SVN
